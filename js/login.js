/**
 * Created by chenfengjuan on 16/10/8.
 */
$().ready(function () {
    // 回车键事件
    // 绑定键盘按下事件
    $(document).keydown(function(e) {
        // 回车键事件
        if(e.which == 13) {
            if($("#formLogin").length>0){
                login();
            }
        }
    });
});
//登陆
function login() {
    if($("#userID").val()==""){
        alert("账号不能为空");
    }else if($("#password").val()==""){
        alert("密码不能为空");
    }else {
        $.post("https://mpmt.gemii.cc/mpmt/sysuser/login",
            {
                username:$("#userID").val(),
                password:$("#password").val()
            },
            function(data){
                if(data.status==400){
                    alert(data.msg);
                }else if(data.status==200){
                    sessionStorage.setItem("name",data.status);
                    window.location.href="index.html";
                }else {
                    alert("登录失败");
                }
            });
    }
}

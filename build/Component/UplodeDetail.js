/**
 * Created by chenfengjuan on 17/2/20.
 */
var UplodeDetail=React.createClass({displayName: "UplodeDetail",
    getInitialState: function () {
        var data=this.requestDetailData();
        return {
            task:data.task,
            imgs:data.imgs
        }
    },
    backManage: function () {
        $('.reviewDetailsCon').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    backTaskMana: function () {
        $('.taskDetails').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    componentDidMount: function () {
        // this.requestDetailData();
        this.setEvent();
    },
    setEvent: function () {
        $(".refuseButton").on("click",this.refuseReview);
        $(".allowButton").on("click",this.allowReview);
    },
    requestDetailData: function () {
        var self=this;
        var returnD;
        $.ajax({
            url: serverip+'tmrecord/web/selectTaskPutDetail',
            type: 'get',
            async: false,
            data:{
                id:this.props.data
            },
            success: function (data) {
                console.log(data.data);
                if(data.status=200){
                    if(data.data.task.state!=undefined){
                        if(data.data.task.state==0){
                            data.data.task.state="任务中";
                        }else if(data.data.task.state==1){
                            data.data.task.state="审核中";
                        }else if(data.data.task.state==2){
                            data.data.task.state="审核通过";
                        }else {
                            data.data.task.state="审核未通过";
                        }
                    }
                    var name=data.data.task.nickName;
                    if(name!=undefined){
                        data.data.task.nickName=GB2312UnicodeConverter.ToGB2312(name);
                    }
                    var remark=data.data.task.remark;
                    if(remark!=undefined){
                        data.data.task.remark=GB2312UnicodeConverter.ToGB2312(remark);
                    }
                    returnD={
                        task:data.data.task,
                        imgs:data.data.images==undefined?[]:data.data.images
                    }
                }else {
                    alert("删除异常");
                }
            }
        });
        return returnD;
    },
    errorfun: function (e) {
        $(e.target).remove();
    },
    allowReview: function(e){
        if(!$(e.target).hasClass("clickedBtn")){
            if(confirm("确认通过吗")){
                this.props.reviewCallback({id:this.props.data,result:"T"});
                this.backManage();
            }
        }else {
            alert("重复点击，审核已通过");
        }

    },
    refuseReview: function (e) {
        if(!$(e.target).hasClass("clickedBtn")){
            if(confirm("确认不通过吗")){
                this.props.reviewCallback({id:this.props.data,result:"F"});
                this.backManage();
            }
        }else {
            alert("重复点击，审核已不通过");
        }
    },
    getRefuseCss: function () {
        if(this.state.task.state=="任务中"){
            return "disabledCss";
        }else if(this.state.task.state=="审核未通过"){
            return "refuseButton clickedBtn";
        }else {
            return "refuseButton";
        }
    },
    getAllowCss: function () {
        if(this.state.task.state=="任务中"){
            return "disabledCss";
        }else if(this.state.task.state=="审核通过"){
            return "allowButton clickedBtn";
        }else {
            return "allowButton";
        }
    },
    render: function () {
        var contentH=parseInt($(".task").innerHeight())-51;
        return (
            React.createElement("div", {className: "taskDetailsTemplate"}, 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backTaskMana}, "任务管理>"), React.createElement("span", {className: "reviewT", onClick: this.backManage}, "任务审核>"), React.createElement("span", {className: "reviewT2"}, "任务上传详情"), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")
                ), 
                React.createElement("div", {className: "taskDetailsContent", style: {height:contentH+'px'}}, 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("div", {className: "rowTitle"}, "任务名称"), React.createElement("div", {id: "reviewTitle", className: "rowCon"}, this.state.task.title)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("div", {className: "rowTitle"}, "状态"), React.createElement("div", {id: "reviewState", className: "rowCon"}, this.state.task.state)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("div", {className: "rowTitle"}, "领取人"), React.createElement("div", {id: "reviewName", className: "rowCon"}, this.state.task.nickName)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("div", {className: "rowTitle"}, "上传时间"), React.createElement("div", {id: "reviewTime", className: "rowCon"}, this.state.task.finishTimeStr)
                    ), 
                    React.createElement("div", {className: "rowTask previewDiv"}, 
                        
                            this.state.imgs.map(function (v, i) {
                            return (
                            React.createElement("img", {key: i, className: "imgPre", onError: this.errorfun, src: v.fileUrl})
                            )
                            }.bind(this))
                        
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("div", {className: "rowTitle reviewSug"}, "任务建议"), 
                        React.createElement("div", {id: "reviewSuggest", className: "rowCon"}, this.state.task.remark)
                    ), 
                    React.createElement("div", {className: "clearfloat"}, " "), 
                    React.createElement("div", {className: "rowTask reviewSubmit"}, 
                        React.createElement("button", {className: this.getRefuseCss()}, "不通过"), 
                        React.createElement("button", {className: this.getAllowCss()}, "通过")
                    )
                )
            )
        );
    }

});
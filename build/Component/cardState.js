/**
 * Created by jiayi.hu on 5/2/17.
 */

var CardState = React.createClass({displayName: "CardState",
    closeModel: function () {
        $('#cardModel').animate({height: '0'}, 0, function () {
            $(this).css({width: '0'}).html("");
        });
    },
    render: function () {
        console.log(this.props.data.data)
        return (
            React.createElement("div", {className: "cardModelTemplate cardState"}, 
                React.createElement("div", {className: "cardModelInside stateInside"}, 
                    React.createElement("div", {className: "acrossTask"}, 
                        React.createElement("div", {className: "cardModelClose stateClose", onClick: this.closeModel}), 
                        React.createElement("span", null, "卡券说明:  "), 
                        React.createElement("span", null, this.props.data.data)
                    )
                )
            )
        )
    }
})
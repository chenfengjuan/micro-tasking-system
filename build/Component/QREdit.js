/**
 * Created by chenfengjuan on 17/3/28.
 */

var QREdit=React.createClass({displayName: "QREdit",
    getInitialState: function(){
        return {
            qr_disc:"",
            qr_scenes:"",
            qr_valid:"永久",
            qr_time:"",
            qr_type:"文字",
            qr_context:"",
            qr_text_context:"",
            // qr_welcome:"T",
            qr_choose:true,
            qr_ImgTextList:[],
            dictSceneList:[],
            isSend:true,
            dictSourceId:""
        }
    },
    requestTaskData: function () {
        $.ajax({
            url:serverip+"dict/web/getDictsByType?type=qrcodescene",
            type:'get',
            success: function (data) {
                if(data.status==200){
                    this.setState(
                        function () {
                            this.state.dictSceneList=data.data;
                            this.state.qr_scenes=data.data[0].id;
                            return this.state;
                        }
                    );
                }
            }.bind(this)
        });
        if(this.props.data!=""){
            $.ajax({
                url:serverip+"iqrcode/web/view?id="+this.props.data,
                type:'get',
                success: function (data) {
                    if(data.status==200){
                        if(data.data.matterFwType=="1"||data.data.matterFwType=="2"){
                            var type=data.data.matterFwType=="1"?"image":"news";
                            $.ajax({
                                url:serverip+"matterfw/web/list?type="+type,
                                type:'get',
                                success: function (typedata) {
                                    if(typedata.status==200){
                                        this.setState({
                                            qr_disc:data.data.description,
                                            qr_scenes:data.data.dictSceneId,
                                            qr_valid:data.data.expiryDay==0?"永久":"短期",
                                            qr_time:data.data.expiryDay,
                                            qr_type:data.data.matterFwType=="1"?"图片":"图文",
                                            qr_context:data.data.matterFwType!="0"?data.data.matterFwId:"",
                                            qr_text_context:data.data.matterFwType=="0"?data.data.matterFwContent:"",
                                            qr_choose:data.data.matterFwType=="0"||data.data.matterFwType=="1"||data.data.matterFwType=="2",
                                            qr_context:typedata.data[0].matterId,
                                            qr_ImgTextList:typedata.data,
                                            dictSourceId:data.data.dictSourceId
                                        });
                                    }
                                }.bind(this)
                            });
                        }else{
                            this.setState(
                                {
                                    qr_disc:data.data.description,
                                    qr_scenes:data.data.dictSceneId,
                                    qr_valid:data.data.expiryDay==0?"永久":"短期",
                                    qr_time:data.data.expiryDay,
                                    qr_type:"文字",
                                    qr_context:data.data.matterFwType=="0"?data.data.matterFwContent:"",
                                    qr_text_context:data.data.matterFwContent==null?"":data.data.matterFwContent,
                                    qr_choose:data.data.matterFwType=="0"||data.data.matterFwType=="1"||data.data.matterFwType=="2",
                                    dictSourceId:data.data.dictSourceId
                                }
                            );
                        }
                    }
                }.bind(this)
            });
        }
    },
    componentDidMount: function () {
        this.requestTaskData();
    },
    backManage: function () {
        $('#newQR').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
        console.log(this.props)
        this.props.callback();
    },
    getMatterfw: function (type,value) {
        $.ajax({
            url:serverip+"matterfw/web/list?type="+type,
            type:'get',
            success: function (data) {
                if(data.status==200){
                    this.setState({
                        qr_type:value,
                        qr_context:data.data[0].matterId,
                        qr_ImgTextList:data.data,
                        qr_text_context:""
                    });
                }
            }.bind(this)
        });
    },
    handleChange: function (e) {
        e.persist();
        if(e.target.name=="qr_type"){
            if(e.target.value=="图片"){
                this.getMatterfw("image","图片");
            }else if(e.target.value=="图文"){
                this.getMatterfw("news","图文");
            }else{
                this.setState({
                    [e.target.name]:e.target.value
                });
            }
        }else if(e.target.name=="qr_time"){
            this.filterTime(e);
            this.setState({
                [e.target.name]:e.target.value
            });
        }else if(e.target.name=="qr_choose"){
            this.setState({
                [e.target.name]:e.target.checked
            });
        }else{
            this.setState({
                [e.target.name]:e.target.value
            });
        }
    },
    filterTime: function (e) {
        e.target.value=e.target.value.replace(/\D/g,'');
        if(e.target.value!=""){

            e.target.value=parseInt(e.target.value);
            if(e.target.value==0){
                e.target.value="";
            }else if(e.target.value>30){
                e.target.value=30;
            }
        }
    },
    saveQR: function (e) {
        console.log(e.target.name);
        console.log(this.state);
        if(this.props.data==""){
            this.addQR();
        }else{
            if(confirm("确定修改吗？")){
                this.updateQR();
            }
        }
    },
    addQR: function () {
        if(this.checkData()&&this.state.isSend){
            this.state.isSend=false;
            $.ajax({
                url:serverip+"iqrcode/web/add",
                type:'post',
                data:this.getFormData(),
                success: function(data){
                    this.state.isSend=true;
                    if(data.status==200){
                        this.backManage();
                    }else if(data.status==400){
                        alert(data.msg);
                    }else if(data.status==500){
                        alert(data.msg);
                    }
                }.bind(this),
                error: function (e) {
                    this.state.isSend=true;
                }.bind(this)
            });
        }
    },
    updateQR: function () {
        if(this.checkData()&&this.state.isSend){
            this.state.isSend=false;
            $.ajax({
                url:serverip+"iqrcode/web/update",
                type:'post',
                data:this.getFormData(),
                success: function(data){
                    this.state.isSend=true;
                    if(data.status==200){
                        this.backManage();
                    }else if(data.status==400){
                        alert(data.msg);
                    }else if(data.status==500){
                        alert(data.msg);
                    }
                }.bind(this),
                error: function (e) {
                    this.state.isSend=true;
                }.bind(this)
            });
        }
    },
    getFormData: function () {
        var data=new Object();
        if(this.props.data!=""){
            data.id=this.props.data;
        }
        if(this.props.data==""){
            data.expiryDay=this.state.qr_time;
            if(this.state.qr_valid=="永久"){
                data.expiryDay=0;
            }
        }
        data.description=this.state.qr_disc;
        if(this.state.qr_choose){
            data.matterFwType=this.state.qr_type=="文字"?0:this.state.qr_type=="图片"?1:2;
            data.matterFwContent=this.state.qr_type=="文字"?this.state.qr_text_context:this.state.qr_context;
        }else{
            data.matterFwType=null;
            data.matterFwContent=null;
        }

        data.dictSceneId=this.state.qr_scenes;
        data.dictSourceId=this.state.dictSourceId;
        $.each(this.state.dictSceneList,function (i, item) {
            if(item.id==this.state.qr_scenes){
                data.sceneValue=item.value;
            }
        }.bind(this));
        console.log(data);
        return data;
    },
    checkData: function () {
        var flag=0;
        $(".QREdit .noneRemind").remove();
        if(this.state.qr_disc==""){
            flag++;
            $(".QREdit input[name='qr_disc']").parent().parent().append('<span class="noneRemind" style="margin-right: 620px;">不能为空</span>');
        }
        if(this.state.qr_valid=="短期"){
            if(this.state.qr_time==""){
                flag++;
                $(".QREdit input[name='qr_time']").parent().parent().append('<span class="noneRemind" style="margin-right: 620px;">不能为空</span>');
            }
        }
        if(this.state.qr_choose&&this.state.qr_type=="文字"){
            if(this.state.qr_text_context==""){
                flag++;
                $(".QREdit textarea[name='qr_text_context']").parent().parent().append('<span class="noneRemind" style="margin-right: 200px;">不能为空</span>');
            }
        }
        if(flag==0){
            return true
        }else{
            return false
        }
    },
    getTimeCSS: function () {
        return this.state.qr_valid=="短期"?"rowTask":"rowTask none-box";
    },
    getTextCSS: function () {
        return this.state.qr_type=="文字"&&this.state.qr_choose?"rowTask":"rowTask none-box";
    },
    getImgCSS: function () {
        return this.state.qr_type!="文字"&&this.state.qr_choose?"rowTask":"rowTask none-box";
    },
    getRemindCSS: function () {
        return this.state.qr_valid=="永久"?"qrRemind":'qrRemind none-box';
    },
    getContextCSS: function () {
        return this.state.qr_choose?"":"none-box";
    },
    render: function(){
        var contentH=parseInt($(".QRcode").innerHeight())-51;
        return (
            React.createElement("div", {className: "taskDetailsTemplate QREdit"}, 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backManage}, "二维码管理 > "), React.createElement("span", {className: "reviewT2"}, this.props.name), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")), 
                React.createElement("div", {className: "taskDetailsContent", style: {height:contentH+'px'}}, 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "二维码名字:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon formConQR"}, React.createElement("input", {name: "qr_disc", value: this.state.qr_disc, onChange: this.handleChange}))
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "投放场景:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon formConQR"}, 
                            React.createElement("select", {className: "taskPlatform_select selectAdd", name: "qr_scenes", value: this.state.qr_scenes, onChange: this.handleChange}, 
                                
                                    this.state.dictSceneList.map(function (value, i) {
                                        return (
                                            React.createElement("option", {key: i, value: value.id}, value.value)
                                        )
                                    })
                                
                            )
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "有效期:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon formConQR"}, 
                            React.createElement("select", {className: "taskPlatform_select selectAdd", disabled: this.props.data!="", name: "qr_valid", value: this.state.qr_valid, onChange: this.handleChange}, 
                                React.createElement("option", {value: "永久"}, "永久"), 
                                React.createElement("option", {value: "短期"}, "短期")
                            )
                        ), 
                        React.createElement("span", {className: this.getRemindCSS()}, React.createElement("span", {className: "qrRemindIcon"}, " "), "永久二维码有个数限制，请大家根据需要选择")
                    ), 
                    React.createElement("div", {className: this.getTimeCSS()}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "短期时长:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon formConQR"}, 
                            React.createElement("input", {name: "qr_time", value: this.state.qr_time, disabled: this.props.data!="", onChange: this.handleChange})
                        ), 
                        React.createElement("span", {className: "qrRemind", style: {marginRight: '548px'}}, React.createElement("span", {className: "qrRemindIcon"}, " "), "从创建日开始计算")
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "选择素材:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "alertQR formConQR", style: {height: this.state.qr_choose?'50px':'34px'}}, 
                            React.createElement("form", null, 
                                React.createElement("input", {type: "checkbox", name: "qr_choose", checked: this.state.qr_choose, style: {marginBottom: '10px'}, onChange: this.handleChange})
                            ), 
                            React.createElement("form", {className: this.getContextCSS()}, 
                                React.createElement("input", {type: "radio", value: "文字", checked: this.state.qr_type=="文字", name: "qr_type", onChange: this.handleChange}), React.createElement("span", {className: "radioText"}, "文字"), 
                                React.createElement("input", {type: "radio", value: "图片", checked: this.state.qr_type=="图片", name: "qr_type", onChange: this.handleChange}), React.createElement("span", {className: "radioText"}, "图片"), 
                                React.createElement("input", {type: "radio", value: "图文", checked: this.state.qr_type=="图文", name: "qr_type", onChange: this.handleChange}), React.createElement("span", null, "图文")
                            )
                        )
                    ), 
                    React.createElement("div", {className: this.getImgCSS()}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "内容:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon formConQR"}, 
                            React.createElement("select", {className: "taskPlatform_select selectAdd", name: "qr_context", value: this.state.qr_context, onChange: this.handleChange}, 
                                
                                    this.state.qr_ImgTextList.map(function (item, i) {
                                        return (React.createElement("option", {key: i, value: item.matterId}, item.title))
                                    })
                                
                            )
                        )
                    ), 
                    React.createElement("div", {className: this.getTextCSS()}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "文字内容:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "alertQR formConQR", style: {height: '55px',width: '704px'}}, 
                            React.createElement("textarea", {name: "qr_text_context", value: this.state.qr_text_context, onChange: this.handleChange, className: "textareaCss taskIntroduction"})
                        )
                    ), 
                    /*<div className="rowTask">*/
                        /*<span className="titlesPosition titleQR">欢迎语:</span><span className="required">＊</span>*/
                        /*<div className="alertQR formConQR">*/
                            /*<form>*/
                                /*<input type="radio" name="qr_welcome" defaultChecked={true} onChange={this.handleChange} defaultValue="T" /><span>推送</span>*/
                                /*<input className="radioInput" type="radio" name="qr_welcome" onChange={this.handleChange} defaultValue="F" /><span>不推送</span>*/
                            /*</form>*/
                        /*</div>*/
                    /*</div>*/
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("br", null), 
                        React.createElement("button", {onClick: this.saveQR, className: "qrSave"}, "保存")
                    )
                )
            ));
    }
});


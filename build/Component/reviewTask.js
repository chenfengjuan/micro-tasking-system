/**
 * Created by chenfengjuan on 17/2/16.
 */
/**
 * Created by chenfengjuan on 17/2/16.
 */
var ReviewTask=React.createClass({displayName: "ReviewTask",
    getInitialState: function () {
        return {
            data:{
                data:[],
                totals:1,
                currentPage:1,
                title:'',
                integral:'',
                count:0
            },
            sort:{
                name: 'receiveTimeStr',
                asc: -1
            },
            pageData:[],
            checkedAll: false
        }
    },
    requestReviewData: function (formData) {
        var self=this;
        var pageTotals=1;
        var rowsData=[];
        $.ajax({
            url: serverip+'tmrecord/web/selectTasksMembers',
            type: 'GET',
            data:formData,
            success: function (data) {
                if(data.status==200){
                    var tmrs = data.data.tmrs;
                    var count=tmrs.length;
                    var SIZE=10;
                    tmrs.forEach(function (o) {
                        o.isChecked = false;
                    });
                    if(count%SIZE==0){
                        pageTotals=parseInt(count/SIZE);
                    }else{
                        pageTotals=parseInt(count/SIZE)+1;
                    }
                    rowsData=self.sortData({data:data.data.tmrs,sort:self.state.sort});
                    self.setState({
                        data:{
                            data:rowsData,
                            totals:pageTotals,
                            currentPage:self.state.data.currentPage,
                            title:data.data.title,
                            integral:data.data.reward,
                            count:count
                        },
                        pageData:self.getpageData({data:rowsData,page:self.state.data.currentPage})
                    });
                }else {
                    alert("数据请求异常");
                }
            }
        });
    },
    getpageData: function (args) {
        var returnData=new Array();
        var SIZE=10;
        var indexStart=(args.page-1)*SIZE;
        var indexEnd=SIZE*args.page;
        returnData=args.data.slice(indexStart,indexEnd);
        return returnData;
    },
    sortData: function (args) {
        var sortedData;
        if(args.data==undefined){
            var data=this.state.data.data;
            if(this.state.sort.name==args.sort.name){
                data.reverse();
                sortedData=data;
            }else{
                sortedData=data.sort(function (a, b) {
                    if(a[args.sort.name] < b[args.sort.name]) return -args.sort.asc;
                    if(a[args.sort.name] > b[args.sort.name]) return args.sort.asc;
                    return 0;
                });
            }
            this.setState({
                data:{
                    data: sortedData,
                    totals: this.state.data.totals,
                    currentPage: 1,
                    title:this.state.data.title,
                    integral:this.state.data.integral,
                    count:this.state.data.count
                },
                sort:{
                    name: args.sort.name,
                    asc: args.sort.asc
                },
                pageData:this.getpageData({data:sortedData,page:1})
            });
        }else {
            sortedData=args.data.sort(function (a, b) {
                if(a[args.sort.name] < b[args.sort.name]) return -args.sort.asc;
                if(a[args.sort.name] > b[args.sort.name]) return args.sort.asc;
                return 0;
            });
            return sortedData;
        }
    },
    backManage: function () {
        $('.taskDetails').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    componentDidMount: function () {
        this.requestReviewData({taskId:this.props.data});
        this.setLayout();
        this.setEvent();
    },
    setEvent: function () {
        $(window).resize(this.setLayout);
        $("#reviewManagement").delegate(".reviewDetails","click",this.reviewDetails);
        $("#reviewManagement").delegate("td .allowReview","click",this.reviewTask);
    },
    reviewDetails: function (e) {
        var reviewID=$(e.target).parent().attr("name");
        var self=this;
        ReactDOM.render(
            React.createElement(UplodeDetail, {data: reviewID, reviewCallback: this.sendMsgRev}),
            $('.reviewDetailsCon')[0]
        );
        $('.reviewDetailsCon').css({width:'100%'}).animate({height:'100%'},400);
        $(window).resize();

    },
    reviewTask: function (e) {
        if(!$(e.target).hasClass("allowCss")){
            var X = $(e.target).offset().top;
            var Y = $(e.target).offset().left;
            if(e.target.name=="no"){
                ReactDOM.render(
                    React.createElement(Confirm, {name: "审核不通过吗？", data: {x:X,y:Y,event:e}, callback: this.refuseConfirmCallback}),
                    $("#maskContainer")[0]
                );
            }else if(e.target.name=="yes"){
                ReactDOM.render(
                    React.createElement(Confirm, {name: "审核通过吗？", data: {x:X,y:Y,event:e}, callback: this.allowConfirmCallback}),
                    $("#maskContainer")[0]
                );
            }
        }else {
            alert("重复点击，审核已"+$(e.target).text());
        }
    },
    refuseConfirmCallback: function (data) {
        if(data.msg){
            this.sendMsgRev({id:$(data.e.target).parent().attr("name"),result:"F"});
            $("#maskContainer").empty();
        }else{
            $("#maskContainer").empty();
        }
    },
    allowConfirmCallback: function (data) {
        if(data.msg){
            this.sendMsgRev({id:$(data.e.target).parent().attr("name"),result:"T"});
            $("#maskContainer").empty();
        }else{
            $("#maskContainer").empty();
        }
    },
    sendMsgRev: function (data) {
        var self=this;
        console.log(data);
        $.ajax({
            url: serverip+'tmrecord/web/verifyTaskPut',
            type: 'post',
            data: data,
            success: function (data) {
                if(data.status==200){
                    self.requestReviewData({taskId:self.props.data});
                }else if(data.status==400){
                    alert(data.msg);
                }else {
                    alert("操作异常");
                }
            }
        });
    },
    setLayout: function () {
        var bodyH=$(window).height();
        var headerH=$(".headerContainer").outerHeight();
        var padding=parseInt($(".contentContainer").css("padding"))*2;
        $("#reviewManagement").css({'maxHeight': bodyH-headerH-padding-173+'px'});
    },
    selectPage: function (page) {
        var pData=this.getpageData({data:this.state.data.data,page:page});
        var isCheckAll = pData.every(function (item) {
            return item.isChecked;
        });
        this.setState({
            data:{
                data:this.state.data.data,
                totals:this.state.data.totals,
                currentPage:page,
                title:this.state.data.title,
                integral:this.state.data.integral,
                count:this.state.data.count
            },
            pageData:pData,
            checkedAll: isCheckAll
        });
    },
    getRefuseCss:function (v) {
        if(v.state==3){
            return "allowReview allowCss"
        }else if(v.state==0){
            return "disabledButton"
        }else{
            return "allowReview"
        }
    },
    getAllowCss: function (v) {
        if(v.state==2){
            return "allowReview allowCss"
        }else if(v.state==0){
            return "disabledButton"
        }else{
            return "allowReview"
        }
    },
    sortReviewData: function (name) {
        var ascSet=this.state.sort.asc;
        if(name==this.state.sort.name){
            ascSet=-this.state.sort.asc;
        }
        this.sortData({sort:{
            name:name,
            asc:ascSet
        }});
    },
    getSortCss: function (name) {
        if(this.state.sort.name!=name){
            return "sorting";
        }else if(this.state.sort.asc==-1){
            return "ascending";
        }else {
            return "descending";
        }
    },
    selectAll: function (e) {
        this.setState({
            checkedAll: !this.state.checkedAll
        });
        var sender = e.target;
        var list = this.state.pageData;
        if(sender.checked){
            list.forEach(function (o) {
                o.isChecked = true;
            });
        }else{
            list.forEach(function (o) {
                o.isChecked = false;
            });
        }
        this.setState({
            pageData: list
        });
    },
    changeStatus: function (v) {
        var pageData = this.state.pageData;
        var index = pageData.indexOf(v);
        pageData[index].isChecked = !pageData[index].isChecked;
        var isCheckAll = pageData.every(function (item) {
            return item.isChecked;
        });
        this.setState({
            pageData: pageData,
            checkedAll: isCheckAll
        });
    },
    batchHandle: function (e) {
        var noChecked = this.state.pageData.every(function (item) {
            return !item.isChecked;
        });
        if(noChecked) {
            alert('请选择要操作的数据项');
            return;
        }
        var X = $(e.target).offset().top;
        var Y = $(e.target).offset().left;
        if(e.target.innerHTML == '批量通过'){
            ReactDOM.render(
                React.createElement(Confirm, {name: "批量审核通过吗？", data: {x:X,y:Y,event:e}, callback: this.batchAllowConfirmCallback}),
                $("#maskContainer")[0]
            );
        }else{
            ReactDOM.render(
                React.createElement(Confirm, {name: "批量审核不通过吗？", data: {x:X,y:Y,event:e}, callback: this.batchRefuseConfirmCallback}),
                $("#maskContainer")[0]
            );
        }
    },
    getCheckedIds: function () {
        var ids = [];
        var arr = $.grep(this.state.pageData, function (item) {
            return item.isChecked && item.state==1;
        });
        for(var i=0; i<arr.length; i++){
            ids.push(arr[i].id);
        }
        return ids;
    },
    batchAllowConfirmCallback: function (data) {
        var ids = this.getCheckedIds();
        if(data.msg) {
            this.batchSendMsgRev({ids: ids, result: "T"});
        }
        $("#maskContainer").empty();
    },
    batchRefuseConfirmCallback: function (data) {
        var ids = this.getCheckedIds();
        if(data.msg) {
            this.batchSendMsgRev({ids: ids, result: "F"});
        }
        $("#maskContainer").empty();
    },
    batchSendMsgRev: function (data) {
        var self = this;
        if(data.ids.length == 0) {
            alert('请选择可操作的数据项');
            return;
        }
        $.ajax({
            url: serverip+'tmrecord/web/verifyTasksPut',
            type: 'post',
            traditional: true,
            data: data,
            success: function (res) {
                if(res.status==200){
                    self.requestReviewData({taskId:self.props.data});
                }else if(res.status==400){
                    alert(res.msg);
                }else {
                    alert("操作异常");
                }
            }
        });
    },
    render: function () {
        var contentH=parseInt($(".task").innerHeight())-51;
        return (
            React.createElement("div", {className: "taskDetailsTemplate"}, 
                React.createElement("div", {className: "reviewDetailsCon"}), 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backManage}, "任务管理>"), React.createElement("span", {className: "reviewT2"}, "任务审核"), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")), 
                React.createElement("div", {className: "taskDetailsContent", style: {height:contentH+'px'}}, 
                    React.createElement("div", {className: "reviewTitleCon"}, React.createElement("span", {className: "titleT"}, "任务名称："), React.createElement("span", null, this.state.data.title), 
                        React.createElement("span", {className: "titleT"}, "领取总人数："), React.createElement("span", null, this.state.data.count), 
                        React.createElement("span", {className: "titleT"}, "任务奖励："), React.createElement("span", null, this.state.data.integral)
                    ), 
                    React.createElement("div", {className: "tableContainer"}, 
                        React.createElement("div", {className: "operates"}, 
                            React.createElement("button", {className: "allowReview allowCss", onClick: function(e){this.batchHandle(e)}.bind(this)}, "批量通过"), 
                            React.createElement("button", {className: "allowReview allowCss", onClick: function(e){this.batchHandle(e)}.bind(this)}, "批量不通过")
                        ), 
                        React.createElement("table", {className: "reviewTable"}, 
                            React.createElement("thead", null, 
                            React.createElement("tr", null, 
                                React.createElement("th", null, React.createElement("input", {type: "checkbox", checked: this.state.checkedAll, onClick: function (e) {this.selectAll(e)}.bind(this)})), 
                                React.createElement("th", null, "序号"), 
                                React.createElement("th", null, "领取人"), 
                                React.createElement("th", {onClick: function(e){this.sortReviewData('receiveTimeStr');}.bind(this), name: "receiveTimeStr"}, "领取时间", React.createElement("span", {className: this.getSortCss('receiveTimeStr')}, " ")), 
                                React.createElement("th", {onClick: function(e){this.sortReviewData('finishTimeStr');}.bind(this), name: "finishTimeStr"}, "提交时间", React.createElement("span", {className: this.getSortCss('finishTimeStr')}, " ")), 
                                React.createElement("th", null, "提交详情"), 
                                React.createElement("th", {onClick: function(e){this.sortReviewData('state');}.bind(this), name: "state"}, "状态", React.createElement("span", {className: this.getSortCss('state')}, " ")), 
                                React.createElement("th", null, "打赏积分"), 
                                React.createElement("th", null, "操作")
                            )
                            ), 
                            React.createElement("tbody", {id: "reviewManagement"}, 
                            
                                this.state.pageData.map(function (v,i) {
                                    var statuStr="任务中";
                                    if(v.state==1){
                                        statuStr="审核中";
                                    }else if(v.state==2){
                                        statuStr="审核通过";
                                    }else if(v.state==3){
                                        statuStr="审核未通过";
                                    }
                                    var index=(this.state.data.currentPage-1)*10;
                                    return(
                                        React.createElement("tr", {key: i}, 
                                            React.createElement("td", null, React.createElement("input", {type: "checkbox", checked: v.isChecked, onClick: function(){this.changeStatus(v)}.bind(this)})), 
                                            React.createElement("td", null, index+i+1), 
                                            React.createElement("td", null, GB2312UnicodeConverter.ToGB2312(v.name)), 
                                            React.createElement("td", null, v.receiveTimeStr), 
                                            React.createElement("td", null, v.finishTimeStr), 
                                            React.createElement("td", {name: v.id}, React.createElement("a", {name: v.id, className: "aCss reviewDetails"}, "查看详情")), 
                                            React.createElement("td", null, statuStr), 
                                            React.createElement("td", null, 0), 
                                            React.createElement("td", {name: v.id}, React.createElement("a", {name: "no", 
                                                               className: this.getRefuseCss(v)}, "不通过"), 
                                                            React.createElement("a", {name: "yes", 
                                                               className: this.getAllowCss(v)}, "通过")))
                                    );}.bind(this))
                            
                            )
                        )
                    ), 
                    React.createElement(PageToggle, {totals: this.state.data.totals, currentPage: this.state.data.currentPage, callbackPage: this.selectPage})
                )
            )
        );
    }
});
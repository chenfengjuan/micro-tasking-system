/**
 * Created by chenfengjuan on 17/3/28.
 */

var BatchExport=React.createClass({displayName: "BatchExport",
    getInitialState: function () {
        return {
                data:{
                    totals:10,
                    currentPage:1,
                    data:[]
                },
                checkedData:[],
                allData:false
        }
    },
    backManage: function () {
        $('#newQR').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    requestTaskData: function (page) {
        var form=this.getFormData(page);
        console.log(form);
        var data=[
            {id:1,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==1"},
            {id:2,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==2"},
            {id:3,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==3"},
            {id:4,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==4"},
            {id:5,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==5"},
            {id:6,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==6"},
            {id:7,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==7"},
            {id:8,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==8"},
            {id:9,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==9"},
            {id:10,qr:"知识卡片",scenes:"知识卡片",valid:"永久",time:'2017-03-22',person:"花开",qrurl:"https://login.weixin.qq.com/qrcode/QdGgcbDWeA==10"}
        ];
        var checkedData=new Object();
        $.each(data,function (i, item) {
            checkedData[item.id]=false;
        });
        this.setState({
            data:{
                totals:10,
                currentPage:page,
                data:data
            },
            checkedData:checkedData,
            allData:false
        });
    },
    componentDidMount: function () {
        this.requestTaskData(1);
        this.setEvent();
        $(window).resize();
    },
    getFormData: function (args) {
        var formData=new Object();
        formData.pageNo =args!=undefined?parseInt(args):1;

        return formData;
    },
    setEvent: function () {
        $("#export_start,#export_end").jeDate({
            format:"YYYY-MM-DD",
            zIndex:3000,
            ishmsVal:true,
            success:function(elem) {
                $("#jedatebox").hide();
                $(window).resize();
                var top=$(elem).offset().top+$(elem).outerHeight();
                var left=$(elem).offset().left;
                $("#jedatebox").css({top:top+'px',left:left+'px'});
                $("#jedatebox").show();
                $(".jedatemm").off('click');
                $(".jedateyy").off('click');
                $(".jedatetodaymonth").remove();
                $(".jedateblue .jedatebot .jedatebtn span").css({width: '46%'});
                $(".pndrop").remove();
            }
        });
    },
    searchData: function () {

    },
    changeCheck: function (e) {
        e.persist();
        if(e.target.name=="allData"){
            if(e.target.checked){
                this.setState(function () {
                    this.state.allData=true;
                    $.each(this.state.checkedData,function (i,item) {
                        this.state.checkedData[i]=true;
                    }.bind(this));
                    return this.state
                });
            }else{
                this.setState(function () {
                    this.state.allData=false;
                    $.each(this.state.checkedData,function (i,item) {
                        this.state.checkedData[i]=false;
                    }.bind(this));
                    return this.state
                });
            }
        }else if(e.target.name=="itemData"){
            this.setState(function () {
                this.state.checkedData[e.target.value]=e.target.checked;
                return this.state
            });
        }

    },
    render: function(){
        var contentH=parseInt($(".QRcode").innerHeight())-51;
        return (
            React.createElement("div", {className: "taskDetailsTemplate QRDetail"}, 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backManage}, "二维码管理 > "), React.createElement("span", {className: "reviewT2"}, this.props.name), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")), 
                React.createElement("div", {className: "taskDetailsContent", style: {height:contentH+'px'}}, 
                    React.createElement("div", {className: "formContainer", style: {width:'auto',marginBottom:'20px'}}, 
                        React.createElement("label", null, "关注时间："), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "export_start", ref: "export_start", type: "text"}), 
                        React.createElement("label", {style: {marginRight: '15px'}}, "－"), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "export_end", ref: "export_end", type: "text"}), 
                        React.createElement("button", {className: "searchButton QRsearch", onClick: this.searchData}, "查询"), 
                        React.createElement("button", {className: "searchButton QRsearch", onClick: this.searchData}, "导出")
                    ), 
                    React.createElement("div", {className: "tableContainer"}, 
                        React.createElement("table", {className: "qrTable"}, 
                            React.createElement("thead", null, 
                            React.createElement("tr", null, 
                                React.createElement("th", null, React.createElement("input", {type: "checkbox", checked: this.state.allData, name: "allData", onChange: this.changeCheck})), 
                                React.createElement("th", null, "序号"), 
                                React.createElement("th", null, "二维码说明"), 
                                React.createElement("th", null, "投放场景"), 
                                React.createElement("th", null, "有效期"), 
                                React.createElement("th", null, "创建时间"), 
                                React.createElement("th", null, "创建人"), 
                                React.createElement("th", null, "素材名")
                            )
                            ), 
                            React.createElement("tbody", null, 
                            
                                this.state.data.data.map(function (value, i) {
                                    return(
                                        React.createElement("tr", {key: i}, 
                                            React.createElement("td", null, React.createElement("input", {type: "checkbox", checked: this.state.checkedData[value.id], value: value.id, name: "itemData", onChange: this.changeCheck})), 
                                            React.createElement("td", null, (this.state.data.currentPage-1)*10+i+1), 
                                            React.createElement("td", null, value.qr), 
                                            React.createElement("td", null, value.scenes), 
                                            React.createElement("td", null, value.valid), 
                                            React.createElement("td", null, value.time), 
                                            React.createElement("td", null, value.person), 
                                            React.createElement("td", null, value.scenes)
                                        )
                                    );
                                }.bind(this))
                            
                            )
                        )
                    ), 
                    React.createElement(PageToggle, {totals: this.state.data.totals, currentPage: this.state.data.currentPage, callbackPage: this.selectPage})
                )
            ));
    }
});


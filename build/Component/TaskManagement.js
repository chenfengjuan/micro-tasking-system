/**
 * Created by chenfengjuan on 17/2/16.
 */

var TaskManagement=React.createClass({displayName: "TaskManagement",
    getInitialState: function(){
        return {
            data: {
                data:[],
                totals:1,
                currentPage:1
            },
            formData:{
                isIncloude: false
            },
            tasktype:[],
        }
    },
    requestTaskData: function (args) {
        var self=this;
        var pageTotals=1;
        var currentPage=1;
        var rowsData=[];
        var formData=this.getFormData(args);
        if(this.checkformData(formData)){
            $.ajax({
                url: serverip+'tasks/web/getTasksManage',
                type: 'GET',
                data:formData,
                success: function (data) {
                    console.log(data);
                    if(data.result){
                        if(data.page.total%data.page.pageSize==0){
                            pageTotals=parseInt(data.page.total/data.page.pageSize);
                        }else{
                            pageTotals=parseInt(data.page.total/data.page.pageSize)+1;
                        }
                        currentPage=data.page.pageNo;
                        rowsData=data.page.rows;
                        self.setState({
                            data:{
                                data:rowsData,
                                totals:pageTotals,
                                currentPage:currentPage
                            }
                        });
                    }else {
                        alert("数据请求异常");
                    }
                }
            });
        }
    },
    checkformData: function (data) {
        if((data.shelveDateStr!="")&&(data.endDateStr!="")){
            var start=new Date(data.shelveDateStr);
            var end=new Date(data.endDateStr);
            if(start>end){
                alert("结束日期必须在开始日期之后");
                return false
            }else {
                return true;
            }
        }else {
            return true;
        }
    },
    componentDidMount: function () {
        var self=this;
        var tasktype=[];
        $.ajax({
            url: serverip+'dict/web/getDictsByType?type=tasktype',
            type: 'GET',
            success:function (data) {
                self.setState({
                    tasktype:data.data
                });
            }
        });
        this.requestTaskData({page:1});
        this.setEvent();
    },
    setEvent: function () {
        $("#taskManagement").delegate("a.modifyTask","click",this.modifyTask);
        $("#taskManagement").delegate("a.reviewTask","click",this.reviewTask);
        $("#taskManagement").delegate("a.onlineBtn","click",this.taskOnline);
        $("#taskManagement").delegate("a.delTask","click",this.taskDel);

    },
    taskDel: function (e) {
        var X = $(e.target).offset().top;
        var Y = $(e.target).offset().left;
        var text=$(e.target).text();
        ReactDOM.render(
            React.createElement(Confirm, {name: "确认"+text+"吗？", data: {x:X,y:Y,event:e,css:'redConfirmOk'}, callback: this.delConfirmCallback}),
            $("#maskContainer")[0]
        );
    },
    delConfirmCallback: function (data) {
        var self=this;
        var isDel="F";
        if($(data.e.target).text()=="删除"){
            isDel="T";
        }
        if(data.msg){
            $.ajax({
                url: serverip+'tasks/web/deleteTask',
                type: 'get',
                data: {
                    id:data.e.target.name,
                    isDel:isDel
                },
                success: function (data) {
                    if(data.status==200){
                        self.requestTaskData({page:self.state.data.currentPage});
                    }else{
                        alert("删除异常");
                    }
                }
            });
            $("#maskContainer").empty();
        }else{
            $("#maskContainer").empty();
        }
    },
    taskOnline: function (e) {
        var X = $(e.target).offset().top;
        var Y = $(e.target).offset().left;
        var text=$(e.target).text();
        ReactDOM.render(
            React.createElement(Confirm, {name: "确认"+text+"吗？", data: {x:X,y:Y,event:e}, callback: this.onlineConfirmCallback}),
            $("#maskContainer")[0]
        );
    },
    onlineConfirmCallback: function (data) {
        var self=this;
        var isShelve="F";
        if($(data.e.target).text()=="上架"){
            isShelve="T";
        }
        if(data.msg){
            $.ajax({
                url: serverip+'tasks/web/shelveTask',
                type: 'get',
                data: {
                    id:data.e.target.name,
                    isShelve:isShelve
                },
                success: function (data) {
                    if(data.status==200){
                        self.requestTaskData({page:self.state.data.currentPage});
                    }else if(data.status==400){
                        alert("当前时间不可上架");
                    }else{
                        alert("更新异常");
                    }
                }
            });
            $("#maskContainer").empty();
        }else{
            $("#maskContainer").empty();
        }
    },
    selectPage: function (page) {
        this.searchData({page:page});
    },
    handleChange: function () {
        // this.searchData(this.state.data.currentPage);
    },
    searchData: function (args) {
        this.requestTaskData(args);
    },
    getFormData: function (args) {
        var formData=new Object();
        formData.title =ReactDOM.findDOMNode(this.refs.taskName).value;
        if(ReactDOM.findDOMNode(this.refs.taskPlatform).value!="all"){
            formData.typeId =ReactDOM.findDOMNode(this.refs.taskPlatform).value;
        }
        if(ReactDOM.findDOMNode(this.refs.isOnline).value!="all"){
            formData.isShelve =ReactDOM.findDOMNode(this.refs.isOnline).value;
        }
        formData.shelveDateStr =ReactDOM.findDOMNode(this.refs.startTime).value;
        formData.endDateStr =ReactDOM.findDOMNode(this.refs.endTime).value;
        formData.isDel =this.state.formData.isIncloude?"":"F";
        formData.pageNo =args.page!=undefined?parseInt(args.page):1;

        return formData;
    },
    reviewTask: function (e) {
        ReactDOM.render(
            React.createElement(ReviewTask, {name: "任务审核", data: e.target.name}),
            $('.taskDetails')[0]
        );
        $('.taskDetails').css({width:'100%'}).animate({height:'100%'},10);
        $(window).resize();
    },
    addTask: function () {
        var self=this;
        ReactDOM.render(
            React.createElement(TaskDetails, {name: "新增任务", data: "new", backfun: function(){self.requestTaskData({page:1})}}),
            $('.taskDetails')[0]
        );
        $('.taskDetails').css({width:'100%'}).animate({height:'100%'},10);
        $(window).resize();
    },
    modifyTask: function (e) {
        if(e.target.title=="T"){
            alert("上架中的任务需要先下架再修改");
        }else {
            var self=this;
            ReactDOM.render(
                React.createElement(TaskDetails, {name: "修改任务", data: e.target.name, backfun: function(){self.requestTaskData({page:self.state.data.currentPage})}}),
                $('.taskDetails')[0]
            );
            $('.taskDetails').css({width:'100%'}).animate({height:'100%'},10);
            $(window).resize();
        }
    },
    handleCheckbox: function (e) {
        this.setState({
            formData:{
                isIncloude: e.target.checked
            }
        });
    },
    render: function(){
        return (
            React.createElement("div", {className: "task"}, 
                React.createElement("div", {className: "taskDetails"}), 
                React.createElement("div", {className: "taskTitle"}, "任务管理"), 
                React.createElement("div", {className: "formContainer"}, 
                    React.createElement("label", null, "任务名称："), React.createElement("input", {ref: "taskName", className: "taskName_input"}), 
                    React.createElement("label", null, "任务类型："), 
                    React.createElement("select", {ref: "taskPlatform", className: "taskPlatform_select", onChange: this.handleChange}, 
                        React.createElement("option", {value: "all"}, "所有"), 
                        
                            this.state.tasktype.map(function (v, i) {
                                return (
                                    React.createElement("option", {key: i, value: v.id}, v.value)
                                );
                            })
                        
                    ), 
                    React.createElement("label", null, "上下架："), 
                    React.createElement("select", {ref: "isOnline", className: "taskPlatform_select", onChange: this.handleChange}, 
                        React.createElement("option", {value: "all"}, "所有"), 
                        React.createElement("option", {value: "T"}, "上架"), 
                        React.createElement("option", {value: "F"}, "下架")
                    )
                ), 
                React.createElement("div", {className: "formContainer"}, 
                    React.createElement("label", null, "上下架起止时间："), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "sTime", ref: "startTime", type: "text", placeholder: "开始时间"}), 
                    React.createElement("label", {style: {paddingRight: '10px'}}, "至"), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "eTime", ref: "endTime", type: "text", placeholder: "结束时间"}), 
                    React.createElement("input", {className: "isIncloude", ref: "isIncloud", type: "checkbox", defaultChecked: false, onChange: this.handleCheckbox}), React.createElement("span", null, "包含已删除任务"), 
                    React.createElement("button", {onClick: this.searchData, className: "searchButton QRsearch", style: {marginLeft:'20px',width:'68px'}}, "查询"), 
                    React.createElement("button", {onClick: this.addTask, className: "addBtn"}, "新增任务")
                ), 
                React.createElement("div", {className: "tableContainer taskmnTable"}, 
                    React.createElement("table", {className: "taskTable"}, 
                        React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, "序号"), 
                            React.createElement("th", null, "任务名称"), 
                            React.createElement("th", null, "任务类型"), 
                            React.createElement("th", null, "任务品牌"), 
                            React.createElement("th", null, "关键字"), 
                            React.createElement("th", null, "任务奖励"), 
                            React.createElement("th", null, "上下架"), 
                            React.createElement("th", null, "创建时间"), 
                            React.createElement("th", null, "操作")
                        )
                        ), 
                        React.createElement("tbody", {id: "taskManagement"}, 
                        
                            this.state.data.data.map(function (v,i) {
                                var isOn="下架";
                                var isOnBtn="上架";
                                var isDelete="恢复";
                                if(v.isShelve=="T"){
                                    isOn="上架";
                                    isOnBtn="下架";
                                }
                                if(v.isDel=="F"){
                                    isDelete="删除";
                                }
                                var a1Css="allowReview reviewTask";
                                var a2Css="allowReview modifyTask";
                                var a3Css="allowReview onlineBtn";
                                if(v.isDel=="T"){
                                    a1Css="disabledButton";
                                    a2Css="disabledButton";
                                    a3Css="disabledButton";
                                }
                                var index=(this.state.data.currentPage-1)*10;
                                return(
                                    React.createElement("tr", {key: i}, React.createElement("td", null, index+1+i), 
                                        React.createElement("td", null, v.title), 
                                        React.createElement("td", null, v.typeName), 
                                        React.createElement("td", null, v.brandName), 
                                        React.createElement("td", null, v.keywordName), 
                                        React.createElement("td", null, v.integration), 
                                        React.createElement("td", null, isOn), 
                                        React.createElement("td", null, v.createTimeStr), 
                                        React.createElement("td", {name: v.id}, React.createElement("a", {name: v.id, className: a1Css}, "领取管理"), React.createElement("a", {name: v.id, className: a2Css, title: v.isShelve}, "修改"), React.createElement("a", {name: v.id, className: a3Css}, isOnBtn), React.createElement("a", {name: v.id, className: "allowReview delTask"}, isDelete)))
                                );}.bind(this))
                        
                        )
                    )
                ), 
                React.createElement(PageToggle, {totals: this.state.data.totals, currentPage: this.state.data.currentPage, callbackPage: this.selectPage})
            ));
    }
});


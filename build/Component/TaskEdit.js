/**
 * Created by chenfengjuan on 17/2/16.
 */
var TaskDetails=React.createClass({displayName: "TaskDetails",
    getInitialState: function () {
        var taskId=this.props.data;
        if(taskId=="new"){
            taskId="";
        }
        returnData=this.requestEditData(taskId);
        console.log(returnData);
        return returnData;
    },
    requestEditData: function (taskId) {
        var self=this;
        var taskData=new Object();
        $.ajax({
            url: serverip+'tasks/web/queryTaskById',
            type: 'GET',
            async:false,
            data:{
                taskId:taskId
            },
            success: function (data) {
                if(data.status==200){
                    var isintegration=false;
                    var iselecReward=false;
                    var isentityReward=false;
                    var isChecked=true;
                    var taskRewardValue="";
                    if(data.data.task!=undefined&&(data.data.task!=null)){
                        if(data.data.task.integration!=null){
                            isintegration=true;
                            taskRewardValue="task_integral";
                        }
                        if((data.data.task.elecRewardId!=null)){
                            iselecReward=true;
                            taskRewardValue="task_elec";
                        }
                        if((data.data.task.entityReward!=null)&&(data.data.task.entityTotal!=null)){
                            isentityReward=true;
                            taskRewardValue="task_entity";
                        }
                        if((data.data.task.isShelveRemind!=undefined)&&(data.data.task.isShelveRemind=="F")){
                            isChecked=false;
                        }
                        taskData={
                            task_title: data.data.task.title,
                            task_type: data.data.task.typeId,
                            task_key: data.data.task.keywordId.split(";"),
                            task_brand: data.data.task.brandId,
                            task_alert: data.data.task.isShelveRemind,
                            task_start: self.getDate(data.data.task.shelveDate),
                            task_end: self.getDate(data.data.task.endDate),
                            task_reward: taskRewardValue,
                            task_integral: isintegration,
                            integral_count: self.nullTo(data.data.task.integration),
                            task_elec: iselecReward,
                            task_elec_name: self.nullTo(data.data.task.elecReward),

                            task_entity: isentityReward,
                            task_entity_name: self.nullTo(data.data.task.entityReward),
                            task_entity_count: self.nullTo(data.data.task.entityTotal),
                            task_link: data.data.task.taskUrl,
                            task_brief: data.data.task.taskBrief,
                            task_detail: data.data.task.detailRemark,
                            task_moreDetail: self.nullTo(data.data.task.moreDetail),
                            task_guide: self.nullTo(data.data.task.operateRemark),
                            task_subintro: self.nullTo(data.data.task.putRemark),
                            task_alert_checked:isChecked,
                            task_testEndTime: self.getDate(data.data.task.applyEndDate),
                            task_testTotal: self.nullTo(data.data.task.applyTotal),
                            isShelve:data.data.task.isShelve
                        }
                    }else{
                        taskData={
                            task_title: "",
                            task_type: data.data.tasktype[0].id,
                            task_key: [data.data.taskkeyword[0].id],
                            task_brand: data.data.taskbrand[0].id,
                            task_alert: "T",
                            task_start: self.getDate(),
                            task_end: self.getEndDate(),
                            task_reward: "task_integral",
                            task_integral: true,
                            integral_count: "",
                            task_elec: false,
                            task_elec_name: "",

                            task_entity: false,
                            task_entity_name: "",
                            task_entity_count: "",
                            task_link: "",
                            task_brief: "",
                            task_detail: "",
                            task_moreDetail: "",
                            task_guide: "",
                            task_subintro: "",
                            task_alert_checked:true,
                            task_testEndTime: self.getDate(),
                            task_testTotal: ""
                        }
                    }
                    returnData={
                        tasktype:data.data.tasktype,
                        taskbrand:data.data.taskbrand,
                        taskkeyword:data.data.taskkeyword,
                        saveId:taskId
                    }
                    returnData=$.extend(returnData,taskData);
                }else {
                    alert("数据请求异常");
                }
            }
        });
        alert("1")
        $.ajax({
            url:serverip+'tereward/web/selectAllElecReward',
            type:'get',
            async:false,
            success: function (res) {
                console.log(res)
                if(res.status==200) {
                    alert("2")
                    returnData.elecRewardList = res.data;
                    returnData.task_elec_name = res.data[0].id;
                }
            }.bind(this)
        })
        return returnData;
    },
    nullTo: function (str) {
        if(str==null){
            return "";
        }else {
            return str;
        }
    },
    getDate: function (value) {
        if(value!=undefined){
            date=new Date(value);
        }else {
            date=new Date();
        }
        var month=parseInt(date.getMonth())+1;
        if(month<10){
            month="0"+month;
        }
        var hour=date.getHours().toString();
        if(hour.length<2){
            hour="0"+hour;
        }
        return date.getFullYear()+"-"+month+"-"+this.getFormatDate(date.getDate())+" "+hour+":00";
    },
    getEndDate: function () {
        var days=7;
        var date = new Date();
        date.setDate(date.getDate() + days);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hour=date.getHours().toString();
        if(hour.length<2){
            hour="0"+hour;
        }
        return date.getFullYear() + '-' + this.getFormatDate(month) + '-' + this.getFormatDate(day)+" "+hour+":00";
    },
    getFormatDate: function (arg) {
        if (arg == undefined || arg == '') {
            return '';
        }
        var re = arg + '';
        if (re.length < 2) {
            re = '0' + re;
        }
        return re;
    },
    backManage: function () {
        this.props.backfun();
        $('.taskDetails').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    componentDidMount: function () {
        var self=this;
        this.setEvent();
        $("#taskKey").multiselect({
            enableFiltering: true,
            onChange: function(option, checked) {
                if($(this.$select).val().length!=0){
                    $('#taskKey').parent().parent().parent().find(".noneRemark").remove();
                }
                if($(this.$select).val().length>3){
                    $('#taskKey').multiselect('deselect', [option[0].value]);
                }
                self.setState({
                    task_key:$(this.$select).val()
                });
            }
        });
    },
    setEvent: function () {
        var self=this;
        var date=new Date();
        var month=parseInt(date.getMonth())+1;
        if(month<10){
            month="0"+month;
        }
        var nowD=date.getFullYear()+"-"+month+"-"+date.getDate()+" "+date.getHours()+":00:00";
        var dateSet={
            format:"YYYY-MM-DD hh:mm",
            zIndex:3000,
            ishmsVal:true,
            success: function (elem) {
                $("#jedatebox").hide();
                $(window).resize();
                var top=$(elem).offset().top+$(elem).outerHeight();
                var left=$(elem).offset().left;
                $("#jedatebox").css({top:top+'px',left:left+'px'});
                $("#jedatebox").show();
                $(".jedatehms").off("click");
                $(".jedatemm").off('click');
                $(".jedateyy").off('click');
                $(".jedatehms").find("li:nth-of-type(2)").find("input").val("00").attr("disabled", "disabled");
                $(".jedatehms").off("click");
                $(".jedatehms").find("li:nth-of-type(2)").find("input").val("00").attr("disabled","disabled");
                $(".jedatetodaymonth").remove();
                $(".jedateblue .jedatebot .jedatebtn span").css({width: '46%'});
                $(".pndrop").remove();
                $(window).resize();
            },
            choosefun: function (elem, val) {
                self.testTime();
                self.setState({
                    [$(elem).attr("name")]: val
                });
            },
            okfun: function (elem, val) {
                self.testTime();
                self.setState({
                    task_start: $("#editStartTime").val(),
                    task_end: $("#editEndTime").val(),
                    task_testEndTime: $("#testEndTime").val()
                });
            },
            clearfun: function (elem, val) {
                self.setState({
                    [$(elem).attr("name")]: ""
                });
            }
        }
        $("#editStartTime,#editEndTime").jeDate($.extend({minDate:nowD},dateSet));
        $("#testEndTime").jeDate($.extend({minDate:'1900-01-01 00:00:00'},dateSet));
    },
    testTime: function () {
        var s=new Date($("#editStartTime").val());
        var e=new Date($("#editEndTime").val());
        var a=new Date($("#testEndTime").val());
        if(($("#editStartTime").val()!="")&&($("#editEndTime").val()!="")){
            if(s<=e){
                $("#editStartTime").parent().parent().find(".noneRemark").remove();
            }else if(s<(new Date())){
                $("#editStartTime").parent().parent().find(".noneRemark").text("开始日期必须在当前日期后");
            }else {
                $("#editStartTime").parent().parent().find(".noneRemark").text("结束日期必须在开始日期之后");
            }
        }
        if($("#testEndTime").val()!=""){
            if((a<=e)&&(a>=s)){
                $("#testEndTime").parent().parent().find(".noneRemark").remove();
            }else {
                $("#testEndTime").parent().parent().find(".noneRemark").text("申领截止日期必须在上下架日期之间");
            }
        }
    },
    toNull: function (str) {
        if(str==""){
            return null;
        }else{
            return str;
        }
    },
    saveTask: function (e) {
        e.persist();
        $(e.target).attr("disabled",true);
        console.log(this.state);
        var postData=new Object();
        postData.title=this.state.task_title;
        postData.typeId=this.state.task_type;
        postData.brandId=this.state.task_brand;
        postData.keywords=this.state.task_key;
        postData.isShelveRemind=this.state.task_alert;
        postData.shelveDateStr =this.state.task_start+":00";
        postData.endDateStr=this.state.task_end+":00";
        postData.taskUrl=this.state.task_link;
        postData.taskBrief=this.state.task_brief;
        postData.detailRemark=this.state.task_detail;
        postData.operateRemark =this.state.task_guide;
        postData.putRemark=this.state.task_subintro;
        if(this.state.task_moreDetail!=""){
            postData.moreDetail=this.state.task_moreDetail;
        }
        if(this.getTypeName()=="2"){
            postData.applyEndDateStr =this.state.task_testEndTime+":00";
            postData.applyTotal=this.toNull(this.state.task_testTotal);
        }
        if(this.state.task_integral){
            postData.integration=this.toNull(this.state.integral_count);
        }
        if(this.state.task_elec){

            postData.elecRewardId=this.toNull(this.state.task_elec_name);
        }
        if(this.state.task_entity){
            postData.entityReward=this.toNull(this.state.task_entity_name);
            postData.entityTotal=this.toNull(this.state.task_entity_count);
        }
      if(e.target.name==""){
            this.saveNewTask(postData,e);
      }else{
            this.saveModifyTask(postData,e);
      }
    },
    saveNewTask: function (postData,e) {
        console.log(postData);
        if(this.checkData(postData)){
            var self=this;
            $.ajax({
                url:serverip+'tasks/web/addTasks',
                type: 'POST',
                data:postData,
                traditional: true,
                success: function (data) {
                    if(data.status==200){
                        $("#successAL").remove();
                        $(".rowTask button").prev().before('<span id="successAL" style="color: #ff0000">保存成功</span>');
                        setTimeout(function () {
                            $("#successAL").remove();
                        },1000);
                        var integral_count=self.state.task_integral?self.state.integral_count:"";
                        var task_elec_name=self.state.task_elec?self.state.task_elec_name:"";

                        var task_entity_name=self.state.task_entity?self.state.task_entity_name:"";
                        var task_entity_count=self.state.task_entity?self.state.task_entity_count:"";
                        self.setState({
                            saveId: data.data.taskId,
                            isShelve: data.data.isShelve,
                            integral_count: integral_count,
                            task_elec_name: task_elec_name,

                            task_entity_name: task_entity_name,
                            task_entity_count: task_entity_count
                        });
                    }else if(data.status==400){
                        alert(data.msg);
                    }else {
                        alert("新建失败，请重新保存");
                    }
                    $(e.target).attr("disabled",false);
                },
                error: function (error) {
                    $(e.target).attr("disabled",false);
                }
            });
        }else {
            $(e.target).attr("disabled",false);
        }
    },
    saveModifyTask: function (postData,e) {
        postData.id=this.state.saveId;
        postData.isShelve=this.state.isShelve;
        console.log(postData);
        if(this.checkData(postData)){
            var self=this;
            $.ajax({
                url:serverip+'tasks/web/updateTask',
                type: 'POST',
                data:postData,
                traditional: true,
                success: function (data) {
                    if(data.status==200){
                        $("#successAL").remove();
                        $(".rowTask button").prev().before('<span id="successAL" style="color: #ff0000">保存成功</span>');
                        setTimeout(function () {
                            $("#successAL").remove();
                        },1000);
                        var integral_count=self.state.task_integral?self.state.integral_count:"";
                        var task_elec_name=self.state.task_elec?self.state.task_elec_name:"";

                        var task_entity_name=self.state.task_entity?self.state.task_entity_name:"";
                        var task_entity_count=self.state.task_entity?self.state.task_entity_count:"";
                        self.setState({
                            integral_count: integral_count,
                            task_elec_name: task_elec_name,

                            task_entity_name: task_entity_name,
                            task_entity_count: task_entity_count
                        });
                    }else if(data.status==400) {
                        alert(data.msg);
                    }else {
                        alert("更新异常，请重新保存");
                    }
                    $(e.target).attr("disabled",false);
                },
                error: function (error) {
                    $(e.target).attr("disabled",false);
                }
            });
        }else {
            $(e.target).attr("disabled",false);
        }
    },
    remarkDataError: function (elem,flag) {
        flag++;
        elem.parent().parent().append('<span class="noneRemark">不能为空</span>');
        return flag;
    },
    applyEndDateStrIs: function (data,flag) {
        var start=new Date(data.shelveDateStr);
        var end=new Date(data.endDateStr);
        var checkTime=new Date(data.applyEndDateStr);
        if((checkTime<start)||(checkTime>end)){
            flag++;
            $("#testEndTime").parent().parent().append('<span class="noneRemark">申领截止日期必须在上下架日期之间</span>');
        }
        return flag
    },
    checkData: function (data) {
        var flag=0;
        $(".rowTask .noneRemark").remove();
        if(data.title==""){
            flag=this.remarkDataError($("input[name='task_title']"),flag);
        }
        if(data.keywords.length==0){
            flag=this.remarkDataError($("select[name='task_key']").parent(),flag);
        }
        if(data.taskUrl==""){
            flag=this.remarkDataError($("input[name='task_link']"),flag);
        }else {
           if(!(/^http/.test(data.taskUrl))){
               flag++;
               $("input[name='task_link']").parent().parent().append('<span class="noneRemark noneLink">链接错误，必须以http开头</span>');
           }
        }
        if(data.moreDetail!=null){
            if(!(/^http/.test(data.moreDetail))){
                flag++;
                $("input[name='task_moreDetail']").parent().parent().append('<span class="noneRemark noneLink">链接错误，必须以http开头</span>');
            }
        }
        if(data.taskBrief==""){
            flag=this.remarkDataError($("textarea[name='task_brief']"),flag);
        }
        if(data.detailRemark==""){
            flag=this.remarkDataError($("textarea[name='task_detail']"),flag);
        }
        if(data.operateRemark==""){
            flag=this.remarkDataError($("textarea[name='task_guide']"),flag);
        }
        if(data.putRemark==""){
            flag=this.remarkDataError($("textarea[name='task_subintro']"),flag);
        }
        if(this.getTypeName()=="2"){
            if(data.applyEndDateStr==":00"){
                flag=this.remarkDataError($("#testEndTime"),flag);
            }else {
                flag=this.applyEndDateStrIs(data,flag);
            }
            if(data.applyTotal==null){
                flag=this.remarkDataError($("#testTotal"),flag);
            }
        }
        if((data.shelveDateStr==":00")||(data.endDateStr==":00")){
            flag=this.remarkDataError($("#editStartTime"),flag);
        }else {
            var s=new Date(data.shelveDateStr);
            var e=new Date(data.endDateStr);
            var now=new Date();
            if((s<now)&&(this.state.saveId=="")){
                flag++;
                $("#editStartTime").parent().parent().append('<span class="noneRemark">开始日期必须在当前日期后</span>');
            }else if(s>e){
                flag++;
                $("#editStartTime").parent().parent().append('<span class="noneRemark">结束日期必须在开始日期之后</span>');
            }
        }
        var remarkStr='<span class="noneRemark noneRemarkM">不能为空</span>';
        if(this.state.task_integral){
            if(data.integration==null){
                flag++;
                $("input[name='integral_count']").after(remarkStr);
            }
        }
        // if(this.state.task_elec){
        //     if((data.elecTotal==null)||(data.elecReward==null)){
        //         flag++;
        //         $("input[name='task_elec_count']").after(remarkStr);
        //     }
        // }
        if(this.state.task_entity){
            if((data.entityReward==null)||(data.entityTotal==null)){
                flag++;
                $("input[name='task_entity_count']").after(remarkStr);
            }
        }
        if(flag>0){
            $(".taskDetailsContent").scrollTop(0);
            return false;
        }else {
            return true;
        }
    },
    cancelAlert: function (e) {
        if(e.target.value!=""){
            if((e.target.name=="task_link")||(e.target.name=="task_moreDetail")){
                if(/^http/.test(e.target.value)){
                    $(e.target).parent().parent().find(".noneRemark").remove();
                }else {
                    $(e.target).parent().parent().find(".noneRemark").text("链接错误，必须以http开头").addClass("noneLink");
                }
            }else {
                if($(e.target).parent().find(".noneRemark").length>0){
                    $(e.target).parent().find(".noneRemark").remove();
                }else{
                    $(e.target).parent().parent().find(".noneRemark").remove();
                }
            }
        }
    },
    handleChange: function (e) {
        if(e.target.name=="task_reward"){
            var checkArr=["task_integral","task_elec","task_entity"];
            checkArr.splice($.inArray(e.target.value,checkArr),1);
            this.setState({
                [e.target.name]:e.target.value,
                [e.target.value]:true,
                [checkArr[0]]:false,
                [checkArr[1]]:false
            });
        }else if((e.target.name=="task_testTotal")||(e.target.name=="integral_count")||(e.target.name=="task_entity_count")){
            this.verInput(e);
            console.log([e.target.name])
            this.setState({
                [e.target.name]:e.target.value
            });
        }else if(e.target.name=="task_brief"){
            var str=e.target.value;
            if(str.length>54){
                str=str.substr(0,54);
                e.target.value=str;
            }
            this.setState({
                [e.target.name]:str
            });
        }else{
            this.setState({
                [e.target.name]:e.target.value
            });
        }
        var checkItem={
            task_title: true,
            task_link: true,
            task_brief: true,
            task_detail: true,
            integral_count: true,
            task_testTotal: true,
            task_moreDetail: true
        }
        if(checkItem[e.target.name]){
            this.cancelAlert(e);
        }
        if((e.target.name=="task_entity_name")||(e.target.name=="task_entity_count")||(e.target.name=="task_elec_name")){
            var ins=$(e.target).parent().find("input");
            if((ins[1].value!="")&&(ins[2].value!="")){
                $(e.target).parent().find(".noneRemark").remove();
            }
        }
    },
    verInput: function (e) {
        e.target.value=e.target.value.replace(/\D/g,'');
        if(e.target.value!=""){
            e.target.value=parseInt(e.target.value);
            if(e.target.value==0){
                e.target.value="";
            }
        }
    },
    getItemCss: function () {
        var name=this.getTypeName()
        return name=="2"?"rowTask":"rowTask none-box";
    },
    getTypeName: function () {
        var name="";
        var self=this;
        this.state.tasktype.map(function (v, i) {
            if(self.state.task_type==v.id){
                name=v.name;
            }
        });
        return name;
    },
    render: function () {
        console.log(this.state)
        var contentH=parseInt($(".task").innerHeight())-51;
        return (
            React.createElement("div", {className: "taskDetailsTemplate"}, 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backManage}, "任务管理 > "), React.createElement("span", {className: "reviewT2"}, this.props.name), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")), 
                React.createElement("div", {className: "taskDetailsContent", style: {height:contentH+'px'}}, 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "任务名称:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon"}, React.createElement("input", {name: "task_title", defaultValue: this.state.task_title, onChange: this.handleChange}))
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "任务类型:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon"}, 
                        React.createElement("select", {className: "taskPlatform_select selectAdd", name: "task_type", defaultValue: this.state.task_type, onChange: this.handleChange}, 
                            
                                this.state.tasktype.map(function (v, i) {
                                    return (
                                        React.createElement("option", {key: i, value: v.id}, v.value)
                                    );
                                })
                            
                        )
                        )
                    ), 
                    React.createElement("div", {className: this.getItemCss()}, 
                        React.createElement("span", {className: "titlesPosition"}, "申领截止日期:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("input", {className: "inputCss", id: "testEndTime", name: "task_testEndTime", readOnly: "readOnly", type: "text", defaultValue: this.state.task_testEndTime, onChange: this.handleChange})
                        )
                    ), 
                    React.createElement("div", {className: this.getItemCss()}, 
                        React.createElement("span", {className: "titlesPosition"}, "申领总数:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("input", {className: "inputCss", id: "testTotal", name: "task_testTotal", defaultValue: this.state.task_testTotal, onChange: this.handleChange, type: "text"})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "关键词:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon mulDiv"}, 
                            React.createElement("select", {className: "taskPlatform_select form-control mulSelect", multiple: "multiple", name: "task_key", id: "taskKey", defaultValue: this.state.task_key}, 
                                
                                    this.state.taskkeyword.map(function (v, i) {
                                        return (
                                            React.createElement("option", {key: i, value: v.id}, v.value)
                                        );
                                    })
                                
                            )
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "任务品牌:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon"}, 
                            React.createElement("select", {className: "taskPlatform_select selectAdd", name: "task_brand", defaultValue: this.state.task_brand, onChange: this.handleChange}, 
                                
                                    this.state.taskbrand.map(function (v, i) {
                                        return (
                                            React.createElement("option", {key: i, value: v.id}, v.value)
                                        );
                                    })
                                
                            )
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "系统消息任务提醒:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("form", null, 
                                React.createElement("input", {type: "radio", name: "task_alert", onChange: this.handleChange, defaultChecked: this.state.task_alert_checked, defaultValue: "T"}), React.createElement("span", null, "提醒"), 
                                React.createElement("input", {className: "radioInput", type: "radio", name: "task_alert", onChange: this.handleChange, defaultChecked: !this.state.task_alert_checked, defaultValue: "F"}), React.createElement("span", null, "不提醒")
                            )
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "上下架起止时间:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("input", {className: "inputCss", id: "editStartTime", name: "task_start", readOnly: "readOnly", defaultValue: this.state.task_start, onChange: this.handleChange, type: "text"}), 
                            React.createElement("label", {style: {paddingRight: '10px'}}, "至"), 
                            React.createElement("input", {className: "inputCss", id: "editEndTime", name: "task_end", readOnly: "readOnly", defaultValue: this.state.task_end, onChange: this.handleChange, type: "text"})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "任务奖励:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("form", null, 
                                React.createElement("div", null, 
                                    React.createElement("input", {type: "radio", defaultValue: "task_integral", name: "task_reward", defaultChecked: this.state.task_integral, onChange: this.handleChange}), React.createElement("span", null, "积分"), 
                                    React.createElement("label", {className: "taskGetMargin", style: {marginLeft: '20px'}}, "积分值"), 
                                    React.createElement("input", {type: "text", disabled: !this.state.task_integral, 
                                           name: "integral_count", value: this.state.integral_count, onChange: this.handleChange, className: "inputCss"})
                                ), 
                                React.createElement("div", {className: "taskFormMar"}, 
                                    React.createElement("input", {type: "radio", defaultValue: "task_elec", defaultChecked: this.state.task_elec, name: "task_reward", onChange: this.handleChange}), React.createElement("span", null, "电子券"), 
                                    React.createElement("label", {className: "taskGetMargin"}, "名称"), 
                                    /*<input type="text" disabled={!this.state.task_elec} name="task_elec_name" value={this.state.task_elec_name} onChange={this.handleChange} className="inputCss"/>*/
                                    React.createElement("select", {className: "taskPlatform_select", disabled: !this.state.task_elec, name: "task_elec_name", defaultValue: this.state.task_elec_name, onChange: this.handleChange}, 
                                        
                                            this.state.elecRewardList.map(function (v, i) {
                                                return (
                                                    React.createElement("option", {key: i, value: v.id}, v.elecName)
                                                );
                                            })
                                        
                                    )
                                    /*<label className="taskGetMargin">数量</label>*/
                                    /*<input type="text" disabled={!this.state.task_elec} name="task_elec_count" value={this.state.task_elec_count} onChange={this.handleChange} className="inputCss"/>*/
                                ), 
                                React.createElement("div", null, 
                                    React.createElement("input", {type: "radio", name: "task_reward", defaultValue: "task_entity", defaultChecked: this.state.task_entity, onChange: this.handleChange}), React.createElement("span", null, "实物"), 
                                    React.createElement("label", {className: "taskGetMargin", style: {marginLeft: '33px'}}, "名称"), 
                                    React.createElement("input", {type: "text", disabled: !this.state.task_entity, name: "task_entity_name", value: this.state.task_entity_name, onChange: this.handleChange, className: "inputCss"}), 
                                    React.createElement("label", {className: "taskGetMargin"}, "数量"), 
                                    React.createElement("input", {type: "text", disabled: !this.state.task_entity, name: "task_entity_count", value: this.state.task_entity_count, onChange: this.handleChange, className: "inputCss"})
                                )
                            )
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "任务链接:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formCon"}, 
                            React.createElement("input", {name: "task_link", defaultValue: this.state.task_link, onChange: this.handleChange, placeholder: "http"})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "任务卡简介:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("textarea", {name: "task_brief", defaultValue: this.state.task_brief, onChange: this.handleChange, className: "textareaCss taskIntroduction"})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "任务描述:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("textarea", {name: "task_detail", defaultValue: this.state.task_detail, onChange: this.handleChange, className: "textareaCss taskIntroduction"})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "了解更多:"), 
                        React.createElement("div", {className: "formCon"}, 
                            React.createElement("input", {placeholder: "http", name: "task_moreDetail", defaultValue: this.state.task_moreDetail, onChange: this.handleChange})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "操作指南:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("textarea", {name: "task_guide", defaultValue: this.state.task_guide, onChange: this.handleChange, className: "textareaCss taskIntroduction"})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition"}, "提交说明:"), React.createElement("span", {className: "required"}, "＊"), 
                        React.createElement("div", {className: "formConAlert"}, 
                            React.createElement("textarea", {name: "task_subintro", defaultValue: this.state.task_subintro, onChange: this.handleChange, className: "textareaCss taskIntroduction"})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask", style: {textAlign: 'center'}}, 
                        React.createElement("br", null), 
                        React.createElement("button", {onClick: this.saveTask, name: this.state.saveId, className: "saveButton newButton"}, "保存")
                    )
                )
            )
        );
    }
});
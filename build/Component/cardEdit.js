/**
 * Created by jiayi.hu on 4/26/17.
 */
var CardEdit = React.createClass({displayName: "CardEdit",
    getInitialState: function () {
      return{
          card_name:"",
          card_url:"",
          card_brand:"",
          card_state:"",
          operateType:"",
          id:this.props.data.id,
          taskBrandList:this.props.data.data
      }
    },
    componentDidMount: function () {
        console.log(this.state.id)
        if(this.state.id==""){
            //新建卡券
            this.setState({
                operateType:"新建卡券",
            })
        }else {
            //修改卡券
            $.ajax({
                url:serverip+'tereward/web/select/'+this.state.id,
                type:'get',
                success: function (res) {
                    console.log(res)
                    if(res.status==200) {
                        this.setState({
                            card_name: res.data.elecName,
                            card_url: res.data.elecUrl,
                            card_brand: res.data.dictBrandId,
                            card_state: res.data.description
                        })
                    }
                }.bind(this)
            })
            this.setState({
                operateType:"修改卡券",
            })
        }
    },
    handleChange: function (e) {
        e.persist();
        this.setState({
            [e.target.name]:e.target.value
        })
    },
    backManage: function () {
        $('#newCard').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
        this.props.callback();
    },
    checkData: function () {
        var flag=0;
        $(".cardEdit .noneRemind").remove();
        if(this.state.card_name==""){
            flag++;
            console.log($(".cardEdit input[name='card_name']"))
            $(".cardEdit input[name='card_name']").parent().parent().append('<span class="noneRemind" style="margin-right: 620px;">不能为空</span>');
        }
        if(this.state.card_url==""){
            flag++;
            $(".cardEdit input[name='card_url']").parent().parent().append('<span class="noneRemind" style="margin-right: 620px;">不能为空</span>');
        }
        if(this.state.card_state==""){
            flag++;
            $(".cardEdit textarea[name='card_state']").parent().parent().append('<span class="noneRemind" style="margin-right: 620px;">不能为空</span>');
        }
        if(flag==0){
            return true
        }else {
            return false
        }
    },
    saveCard: function () {
        var form = this.getFormData();
        console.log(form)
        if(this.state.id==""){
            alert("addCard")
            this.addCard(form);

        }else {
            if(confirm("确定修改吗?")){
                // this.updateQR();
                alert("updateCard")
                form.id = this.state.id;
                console.log(form)
                this.updateCard(form)
            }
        }
    },
    addCard: function (form) {
        if(this.checkData()) {
            $.ajax({
                url: serverip + 'tereward/web/insert',
                type: 'post',
                data: form,
                success: function (res) {
                    if (res.status == 200) {
                        this.backManage();
                    }
                }.bind(this)
            })
        }
    },
    updateCard: function (form) {
        if(this.checkData()) {
            $.ajax({
                url: serverip + 'tereward/web/update',
                type: 'post',
                data: form,
                success: function (res) {
                    if (res.status == 200) {
                        this.backManage();
                    }
                }.bind(this)
            })
        }
    },
    getFormData: function () {
      var formData = new Object();
        formData.elecName = ReactDOM.findDOMNode(this.refs.cardName).value;
        formData.elecUrl = ReactDOM.findDOMNode(this.refs.cardUrl).value;
        formData.dictBrandId = ReactDOM.findDOMNode(this.refs.cardBrand).value;
        formData.description = ReactDOM.findDOMNode(this.refs.cardState).value;
        return formData;
    },
    render: function () {
        console.log(this.state.taskBrandList)
        return(
           React.createElement("div", {className: "taskDetailsTemplate cardEdit"}, 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backManage}, "卡券管理 > "), React.createElement("span", {className: "reviewT2"}, this.state.operateType), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")
                ), 
                React.createElement("div", {className: "taskDetailsContent"}, 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "卡券名称"), React.createElement("span", {className: "required"}, "*"), 
                        React.createElement("div", {className: "formCon formConQR"}, React.createElement("input", {name: "card_name", value: this.state.card_name, ref: "cardName", onChange: this.handleChange}))
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "卡券URL"), React.createElement("span", {className: "required"}, "*"), 
                        React.createElement("div", {className: "formCon formConQR"}, React.createElement("input", {name: "card_url", value: this.state.card_url, ref: "cardUrl", onChange: this.handleChange}))
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "卡券品牌"), React.createElement("span", {className: "required"}, "*"), 
                        React.createElement("div", {className: "formCon formConQR"}, 
                            React.createElement("select", {className: "taskPlatform_select selectAdd", name: "card_brand", value: this.state.card_brand, ref: "cardBrand", onChange: this.handleChange}, 
                                
                                    this.state.taskBrandList.map(function (value,i) {
                                        return (
                                        React.createElement("option", {key: i, value: value.id}, value.value)
                                        )
                                    })
                                
                            )
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "说明"), React.createElement("span", {className: "required"}, "*"), 
                        React.createElement("div", {className: "alertQR formConQR", style: {height: '55px',width: '704px'}}, 
                            React.createElement("textarea", {className: "textareaCss taskIntroduction", name: "card_state", value: this.state.card_state, ref: "cardState", onChange: this.handleChange})
                        )
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("button", {onClick: this.saveCard, className: "qrSave"}, "保存")
                    )
                )
            )
        )
    }
})
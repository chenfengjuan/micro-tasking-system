/**
 * Created by jiayi.hu on 5/2/17.
 */

var CardDetail = React.createClass({displayName: "CardDetail",
    getInitialState: function () {
        var res = this.props.data;
        console.log(this.props)
        var totals=res.data.total%res.data.pageSize==0?res.data.total/res.data.pageSize:parseInt(res.data.total/res.data.pageSize)+1;
        console.log(totals)
        return{

                totals: totals,
                currentPage: res.data.pageNo,
                data: res.data.rows,
                id:res.id
        }
    },
    selectPage: function (page) {
        this.requestDetail(page);
    },
    requestDetail: function (page) {
        var id = this.state.id;
        var pageNo = page != undefined ? page : 1;
        $.ajax({
            url:serverip+'tereward/web/selectElecRecord/'+pageNo+'/'+id,
            type:'get',
            success:function (res) {
                console.log(res)
                if(res.status==200){
                    // ReactDOM.render(
                    //     <CardDetail name="cardDetail" data={{data:res.data}}/>,
                    //     $('#cardModel')[0]
                    // );
                    // $('#cardModel').css({width:'100%'}).animate({height:'100%'},0);
                    this.setState({
                        currentPage: res.data.pageNo,
                        data: res.data.rows,
                    })
                }
            }.bind(this)
        })
    },
    closeModel: function () {
        $('#cardModel').animate({height:'0'},0,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    dataDownload: function (e) {
        var id = e.target.name;
        window.location.href = serverip+'/elec/sysExport/exportElecRecord?idStr='+id;
    },
    render: function () {
        console.log(this.state)
        return(
            React.createElement("div", {className: "cardModelTemplate cardDetail"}, 
                React.createElement("div", {className: "cardModelInside detailInside"}, 
                        React.createElement("div", {className: "cardTableContainer"}, 
                            React.createElement("div", {className: "cardModelClose", onClick: this.closeModel}), 
                            React.createElement("table", {className: "detailTable"}, 
                                React.createElement("thead", null, 
                                React.createElement("tr", null, 
                                    React.createElement("th", null, "openID"), 
                                    React.createElement("th", null, "昵称"), 
                                    React.createElement("th", null, "点击次数"), 
                                    React.createElement("th", null, "最后点击时间")
                                )
                                ), 
                                React.createElement("tbody", null, 
                                
                                    this.state.data.map(function (value,i) {
                                        console.log(i)
                                        return(
                                            React.createElement("tr", {key: i}, 
                                                React.createElement("td", null, value.openId), 
                                                React.createElement("td", null, value.nickname), 
                                                React.createElement("td", null, value.count), 
                                                React.createElement("td", null, value.createTimeStr)
                                            )
                                        )
                                    })
                                
                                )
                            )
                        ), 
                        React.createElement("div", {className: "cardTableContainer"}, 
                            React.createElement("input", {type: "button", className: "detailExportCss", name: this.state.id, onClick: this.dataDownload, value: "数据导出"})
                        ), 
                    React.createElement(PageToggle, {totals: this.state.totals, currentPage: this.state.currentPage, callbackPage: this.selectPage})
                )
                )
        )
    }
})
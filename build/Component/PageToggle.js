/**
 * Created by chenfengjuan on 17/2/16.
 */
var PageToggle=React.createClass({displayName: "PageToggle",
    handleClick: function (e) {
        var page=$(e.target).attr("href").replace("#","");
        this.props.callbackPage(page);
    },
    getButtonCss: function (v) {
        return v==this.props.currentPage?"pageButton selected":"pageButton";
    },
    getPageArray: function (count,currentIndex) {
        var startIndex=1;
        var endIndex=1;
        var pageArray=new Array();
        if(count>7){
            startIndex=currentIndex-3<1?1:currentIndex-3;
            endIndex=currentIndex+3>count?count:currentIndex+3;
            if(endIndex-startIndex<6){
                if(startIndex==1){
                    endIndex=startIndex+6;
                }else{
                    startIndex=endIndex-6;
                }
            }
        }else{
            startIndex=1;
            endIndex=count;
        }
        for(var i=startIndex;i<=endIndex;i++){
            pageArray.push(i);
        }
        return pageArray;
    },
    getToggleCss: function () {
        var count=parseInt(this.props.totals);
        return count>1?"pageToggle":"pageToggle none-box"
    },
    render: function () {
        var that=this;
        var count=parseInt(this.props.totals);
        var currentIndex=parseInt(this.props.currentPage);
        var pageArray=this.getPageArray(count,currentIndex);
        return (
            React.createElement("div", {className: "pagecom"}, 
                React.createElement("div", {className: "pageInfo"}, 
                    React.createElement("label", {className: "pageCurrentIndex"}, this.props.currentPage), React.createElement("label", null, "/"), React.createElement("label", {className: "pageTotals"}, this.props.totals)
                ), 
                React.createElement("div", {className: this.getToggleCss()}, 
                    React.createElement("a", {className: "pageButton", onClick: this.handleClick, href: "#1"}, "首页"), 
                    React.createElement("a", {className: "pageButton", onClick: this.handleClick, href: '#'+(parseInt(this.props.currentPage)-1>0?parseInt(this.props.currentPage)-1:1)}, "上一页"), 
                    React.createElement("span", null, 
                
                    pageArray.map(function (v, i) {
                    return(React.createElement("a", {key: i, className: that.getButtonCss(v), onClick: that.handleClick, href: '#'+v}, v));
                })
                
                    ), 
                    React.createElement("a", {className: "pageButton", onClick: this.handleClick, href: '#'+(parseInt(this.props.currentPage)+1>parseInt(this.props.totals)?parseInt(this.props.totals):parseInt(this.props.currentPage)+1)}, "下一页"), 
                    React.createElement("a", {className: "pageButton", onClick: this.handleClick, href: '#'+this.props.totals}, "尾页")
                )
            )
        );
    }
});
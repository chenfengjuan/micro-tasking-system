/**
 * Created by chenfengjuan on 17/1/19.
 */
var TabsControl = React.createClass({displayName: "TabsControl",
    getInitialState: function(){
        return {currentIndex: 0,
            childIndex:-1,
            1:false,
            3:false,
        }
    },
    getTitleItemCssClasses: function(element,index){
        return index === this.state.currentIndex ? "active" : "";
    },
    getTitleItemSpanCssClasses: function (element,index) {
        var s=this.state[index] ? "openChild openChildActive" : "openChild";
        return s;
    },
    getContentItemCssClasses: function(index){
        return index === this.state.currentIndex ? "tab-content-item active" : "tab-content-item";
    },
    getContentChildItemCssClasses: function(index){
        return index === this.state.childIndex ? "tab-content-item active" : "tab-content-item";
    },
    getTitleChildItemCssClasses: function (parentIndex,index) {
        if(this.state.currentIndex==parentIndex){
            return index === this.state.childIndex ? "active" : "";
        }else {
            return "";
        }

    },
    startRippleAnimate: function (e, target) {
        var offset = target.offset();
        var x = e.pageX;
        var y = e.pageY;
        var $ripple = $('<span class="ripple"></span>');
        target.find(".ripple").remove();
        $ripple.appendTo(target).css({
            left: x - offset.left - $ripple.width() / 2,
            top: y - offset.top - $ripple.height() / 2
        });
        setTimeout(function () {
            target.find(".ripple").remove();
        },2000);
    },
    itemClick: function (e) {
        e.stopPropagation();
        var target=$(e.target).prop("tagName")=="SPAN"?$(e.target).parent():$(e.target);
        if(target.prop("tagName")=="LI"){
            var index=target.index();
            this.startRippleAnimate(e, target);
            this.setState({currentIndex: index,childIndex:-1});
        }
    },
    itemChildClick: function (e) {
        e.stopPropagation();
        var target=$(e.target).prop("tagName")=="SPAN"?$(e.target).parent():$(e.target);
        if(target.prop("tagName")=="LI"){
            var index=target.index();
            var parentIndex=target.parent().parent().index();
            this.startRippleAnimate(e, target);
            this.setState({currentIndex:parentIndex,childIndex: index});
        }
    },
    itemParentClick: function (e) {
        var target=$(e.target).prop("tagName")=="SPAN"?$(e.target).parent():$(e.target);
        if(target.prop("tagName")=="LI"){
            var index=target.index();
            this.startRippleAnimate(e, target);
            this.setState({
                [index]:!this.state[index]
            });
        }

    },
    render: function(){
        var that = this;
        return (
            React.createElement("div", {className: "height100"}, 
                React.createElement("nav", {className: "sideContainer"}, 
                    React.createElement("ul", null, 
                        React.Children.map(this.props.children, function(element, index){
                            if(element.props.type=="parent"){
                                return (React.createElement("li", {onClick: that.itemParentClick, 
                                            className: that.getTitleItemCssClasses(element,index)}, 
                                    React.createElement("span", null, element.props.name), React.createElement("span", {className: that.getTitleItemSpanCssClasses(element,index)}, "  "), 
                                    React.createElement("ul", {className: that.state[index]?"":"close"}, 
                                        
                                            React.Children.map(element.props.children,function (e, i) {
                                                return (React.createElement("li", {onClick: that.itemChildClick, className: that.getTitleChildItemCssClasses(index,i)}, React.createElement("span", null, e.props.name)))
                                            })
                                        
                                    )
                                ))
                            }else{
                                return (React.createElement("li", {onClick: that.itemClick, className: that.getTitleItemCssClasses(element,index)}, React.createElement("span", null, element.props.name)))
                            }
                        })
                    )
                ), 
                React.createElement("div", {className: "contentContainer"}, 
                    React.Children.map(this.props.children, function(element, index){
                        if(element.props.type=="parent"){
                            return (React.createElement("div", {className: that.getContentItemCssClasses(index)}, 
                                React.Children.map(element.props.children,function (e, i) {
                                    return (React.createElement("div", {className: that.getContentChildItemCssClasses(i)}, e))
                                })
                            ))
                        }else {
                            return (React.createElement("div", {className: that.getContentItemCssClasses(index)}, element))
                        }
                    })
                )
            )
        )
    }
});
var Tab = React.createClass({displayName: "Tab",
    render: function(){
        return (this.props.children);
    }
});

var Content = React.createClass({displayName: "Content",
    render:function(){
       return (
            React.createElement(TabsControl, null, 
                React.createElement(Tab, {name: "任务管理"}, 
                    React.createElement(TaskManagement, null)
                ), 
                React.createElement(Tab, {name: "积分商城"}, 
                    React.createElement("div", {className: "height100 width100"}, "积分商城")
                ), 
                React.createElement(Tab, {name: "积分管理"}, 
                    React.createElement("div", {className: "height100 width100"}, "积分管理")
                ), 
                React.createElement(Tab, {name: "参数管理"}, 
                    React.createElement("div", {className: "height100 width100"}, "参数管理")
                ), 
                React.createElement(Tab, {name: "二维码管理"}, 
                   React.createElement(QRcode, null)
                ), 
                React.createElement(Tab, {name: "卡券管理"}, 
                   React.createElement(CardManage, null)
                )
            )
        );
    }
});



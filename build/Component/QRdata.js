/**
 * Created by chenfengjuan on 17/3/28.
 */

var QRdata=React.createClass({displayName: "QRdata",
    getInitialState: function () {
        return {
            data:{
                totals:1,
                currentPage:1,
                data:[]
            }
        }
    },
    backManage: function () {
        $('#newQR').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    componentDidMount: function () {
        this.requestData(1);
        this.setEvent();
    },
    requestData: function (page) {
        var page=page==undefined?1:page;
        $.ajax({
            url:serverip+"iqrcode/web/listScan",
            type:'get',
            data: this.getFormData(page),
            success: function (data) {
                if(data.result){
                    var totals=data.page.total%data.page.pageSize==0?data.page.total/data.page.pageSize:parseInt(data.page.total/data.page.pageSize)+1;
                    this.setState({
                        data:{
                            totals:totals,
                            currentPage:data.page.pageNo,
                            data:data.page.rows
                        }
                    });
                }
            }.bind(this)
        });
    },
    getFormData: function (page) {
        return {
            id:this.props.data.id,
            attTimeStartStr:ReactDOM.findDOMNode(this.refs.qrdata_start).value,
            attTimeEndStr:ReactDOM.findDOMNode(this.refs.qrdata_end).value,
            pageNo:page
        }
    },
    setEvent: function () {
        $("#qrdata_start,#qrdata_end").jeDate({
            format:"YYYY-MM-DD hh:mm",
            zIndex:3000,
            ishmsVal:true,
            success:function(elem) {
                $("#jedatebox").hide();
                $(window).resize();
                var top=$(elem).offset().top+$(elem).outerHeight();
                var left=$(elem).offset().left;
                $("#jedatebox").css({top:top+'px',left:left+'px'});
                $("#jedatebox").show();
                $(".jedatehms").off("click");
                $(".jedatemm").off('click');
                $(".jedateyy").off('click');
                $(".jedatehms").find("li:nth-of-type(2)").find("input").val("00").attr("disabled", "disabled");
                $(".jedatehms").off("click");
                $(".jedatehms").find("li:nth-of-type(2)").find("input").val("00").attr("disabled","disabled");
                $(".jedatetodaymonth").remove();
                $(".jedateblue .jedatebot .jedatebtn span").css({width: '46%'});
                $(".pndrop").remove();
                $(window).resize();
            }
        });
    },
    searchData: function () {
        this.requestData(1);
    },
    outData: function () {
        window.location.href=serverip+"iqrcode/sysExport/fans?idStr="+this.props.data.id+"&attTimeStartStr="+ReactDOM.findDOMNode(this.refs.qrdata_start).value+"&attTimeEndStr="+ReactDOM.findDOMNode(this.refs.qrdata_end).value;
    },
    render: function(){
        var contentH=parseInt($(".QRcode").innerHeight())-51;
        return (
            React.createElement("div", {className: "taskDetailsTemplate QRDetail"}, 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backManage}, "二维码管理 > "), React.createElement("span", {className: "reviewT2"}, this.props.name), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")), 
                React.createElement("div", {className: "taskDetailsContent", style: {height:contentH+'px'}}, 
                    React.createElement("div", {className: "formContainer", style: {width:'auto',marginBottom:'20px'}}, 
                        React.createElement("label", null, "关注时间："), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "qrdata_start", ref: "qrdata_start", type: "text"}), 
                        React.createElement("label", {style: {marginRight: '15px'}}, "－"), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "qrdata_end", ref: "qrdata_end", type: "text"}), 
                        React.createElement("button", {className: "searchButton QRsearch", onClick: this.searchData}, "查询"), 
                        React.createElement("button", {className: "searchButton QRsearch", onClick: this.outData}, "导出")
                    ), 
                    React.createElement("div", {className: "tableContainer"}, 
                        React.createElement("table", {className: "qrDataTable"}, 
                            React.createElement("thead", null, 
                            React.createElement("tr", null, 
                                React.createElement("th", null, "序号"), 
                                React.createElement("th", null, "open ID"), 
                                React.createElement("th", null, "昵称"), 
                                React.createElement("th", null, "扫码次数"), 
                                React.createElement("th", null, "关注时间")
                            )
                            ), 
                            React.createElement("tbody", null, 
                            
                                this.state.data.data.map(function (value, i) {
                                    return (
                                        React.createElement("tr", {key: i}, 
                                            React.createElement("td", null, (this.state.data.currentPage-1)*10+i+1), 
                                            React.createElement("td", null, value.openid), 
                                            React.createElement("td", null, GB2312UnicodeConverter.ToGB2312(value.nickname)), 
                                            React.createElement("td", null, value.scanCount), 
                                            React.createElement("td", null, value.attTimeStr)
                                        )
                                    )
                                }.bind(this))
                            
                            )
                        )
                    ), 
                    React.createElement(PageToggle, {totals: this.state.data.totals, currentPage: this.state.data.currentPage, callbackPage: this.selectPage})
                )
            ));
    }
});


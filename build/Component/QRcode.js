/**
 * Created by chenfengjuan on 17/3/28.
 */

var QRcode=React.createClass({displayName: "QRcode",
    getInitialState: function(){
        return {
            data:{
                totals:1,
                currentPage:1,
                data:[]
            },
            isIncloude:false,
            checkedData:[],
            allData:false,
            dictSceneList:[]
        }
    },
    requestTaskData: function (page) {
        console.log(page)
        var form=this.getFormData(page);
        $.ajax({
            url:serverip+"iqrcode/web/list",
            type:'get',
            data:form,
            success: function (data) {
                if(data.result){
                    var checkedData=new Object();
                    $.each(data.page.rows,function (i, item) {
                        checkedData[item.id]=false;
                    });
                    var totals=data.page.total%data.page.pageSize==0?data.page.total/data.page.pageSize:parseInt(data.page.total/data.page.pageSize)+1;
                    this.setState({
                        data:{
                            totals:totals,
                            currentPage:data.page.pageNo,
                            data:data.page.rows
                        },
                        checkedData:checkedData,
                        allData:false
                    });
                }
            }.bind(this)
        });
    },
    componentDidMount: function () {
        $.ajax({
            url:serverip+"dict/web/getDictsByType?type=qrcodescene",
            type:'get',
            success: function (data) {
                if(data.status==200){
                    this.setState(
                        function () {
                            this.state.dictSceneList=data.data;
                            return this.state;
                        }
                    );
                }
            }.bind(this)
        });
        this.requestTaskData(1);
        this.setEvent();
    },
    setEvent: function () {
        //配置时间控件
        $("#creatTime").jeDate({
            format:"YYYY-MM-DD",
            zIndex:3000,
            ishmsVal:true,
            success:function(elem) {
                $("#jedatebox").hide();
                $(window).resize();
                var top=$(elem).offset().top+$(elem).outerHeight();
                var left=$(elem).offset().left;
                $("#jedatebox").css({top:top+'px',left:left+'px'});
                $("#jedatebox").show();
                $(".jedatemm").off('click');
                $(".jedateyy").off('click');
                $(".jedatetodaymonth").remove();
                $(".jedateblue .jedatebot .jedatebtn span").css({width: '46%'});
                $(".pndrop").remove();
            }
        });
        $("#export_start,#export_end").jeDate({
            format:"YYYY-MM-DD hh:mm",
            zIndex:3000,
            ishmsVal:true,
            success:function(elem) {
                $("#jedatebox").hide();
                $(window).resize();
                var top=$(elem).offset().top+$(elem).outerHeight();
                var left=$(elem).offset().left;
                $("#jedatebox").css({top:top+'px',left:left+'px'});
                $("#jedatebox").show();
                $(".jedatehms").off("click");
                $(".jedatemm").off('click');
                $(".jedateyy").off('click');
                $(".jedatehms").find("li:nth-of-type(2)").find("input").val("00").attr("disabled", "disabled");
                $(".jedatehms").off("click");
                $(".jedatehms").find("li:nth-of-type(2)").find("input").val("00").attr("disabled","disabled");
                $(".jedatetodaymonth").remove();
                $(".jedateblue .jedatebot .jedatebtn span").css({width: '46%'});
                $(".pndrop").remove();
                $(window).resize();
            }
        });
    },
    getFormData: function (args) {
        var formData=new Object();
        formData.description =ReactDOM.findDOMNode(this.refs.qrDescrip).value;
        formData.dictSceneId =ReactDOM.findDOMNode(this.refs.scenes).value;
        if(formData.dictSceneId=="全部"){
            formData.dictSceneId=null;
        }
        if(ReactDOM.findDOMNode(this.refs.validPeriod).value!="全部"){
            formData.expiryDay =ReactDOM.findDOMNode(this.refs.validPeriod).value;
        }
        formData.createTimeStr =ReactDOM.findDOMNode(this.refs.creatTime).value;
        formData.attTimeStartStr =ReactDOM.findDOMNode(this.refs.export_start).value;
        formData.attTimeEndStr =ReactDOM.findDOMNode(this.refs.export_end).value;
        formData.isDel =this.state.isIncloude?null:"F";
        formData.pageNo =args!=undefined?parseInt(args):1;

        return formData;
    },
    changeCheck: function (e) {
        e.persist();
        if(e.target.name=="isIncloude"){
            this.setState({
                [e.target.name]:e.target.checked
            });
        }else if(e.target.name=="allData"){
            if(e.target.checked){
                this.setState(function () {
                    this.state.allData=true;
                    $.each(this.state.checkedData,function (i,item) {
                        this.state.checkedData[i]=true;
                    }.bind(this));
                    return this.state
                });
            }else{
                this.setState(function () {
                    this.state.allData=false;
                    $.each(this.state.checkedData,function (i,item) {
                        this.state.checkedData[i]=false;
                    }.bind(this));
                    return this.state
                });
            }
        }else if(e.target.name=="itemData"){
            this.setState(function () {
                this.state.checkedData[e.target.value]=e.target.checked;
                return this.state
            });
        }

    },

    selectPage: function (page) {
        this.requestTaskData(page);
    },

    searchData: function (e) {
        this.requestTaskData(1);
    },

    batchDown: function (e) {
        console.log(1);
        $.each(this.state.data.data,function (i, item) {
            if(this.state.checkedData[item.id]){
                $(".qrTable").find(".downFile[name='"+item.id+"']")[0].click();
            }
        }.bind(this));
    },

    batchDel: function (e) {
        $.each(this.state.data.data,function (i, item) {
            if(this.state.checkedData[item.id]){
                this.delQRAjax({
                    id:item.id,
                    isDel:"T"
                });
            }
        }.bind(this));
    },

    batchOut: function (e) {
        var ids=new Array();
        $.each(this.state.data.data,function (i, item) {
            if(this.state.checkedData[item.id]){
                ids.push(item.id);
            }
        }.bind(this));
        ids=ids.join(",");
        window.location.href=serverip+"iqrcode/sysExport/fans?idStr="+ids+"&attTimeStartStr="+ReactDOM.findDOMNode(this.refs.export_start).value+"&attTimeEndStr="+ReactDOM.findDOMNode(this.refs.export_end).value;
    },

    newQR: function (e) {
        if(!$(e.target).hasClass("isdel")){
            var id=e.target.name==undefined?"":e.target.name;
            var name=id==""?"新增二维码":"修改二维码"
            ReactDOM.render(
                React.createElement(QREdit, {name: name, data: id, callback: this.requestTaskData}),
                $('#newQR')[0]
            );
            $('#newQR').css({width:'100%'}).animate({height:'100%'},10);
        }
    },
    delQR: function (e) {
        if(!$(e.target).hasClass("isdel")){
            var X = $(e.target).offset().top;
            var Y = $(e.target).offset().left;
            var text=$(e.target).text();
            var event={target:$(e.target)}
            ReactDOM.render(
                React.createElement(Confirm, {name: "确认"+text+"吗？", data: {x:X,y:Y,event:event,css:'redConfirmOk'}, callback: this.delConfirmCallback}),
                $("#maskContainer")[0]
            );
        }

    },
    delConfirmCallback: function (data) {
        var self=this;
        var getdata={
            id:data.e.target.attr('name'),
            isDel:"T"
        }
        if(data.msg){
            self.delQRAjax(getdata);
            $("#maskContainer").empty();
        }else{
            $("#maskContainer").empty();
        }
    },
    delQRAjax: function (data) {
        $.ajax({
            url: serverip+'iqrcode/web/del',
            type: 'post',
            data:data,
            success: function (data) {
                if(data.status==200){
                    this.requestTaskData(this.state.data.currentPage);
                }else if(data.status==400){
                    alert(data.msg);
                }else if(data.status==500){
                    alert(data.msg);
                }
            }.bind(this)
        });
    },
    lookDetail: function (e) {
        var id=e.target.name;
        $.ajax({
            url:serverip+"iqrcode/web/view?id="+id,
            type:'get',
            success: function (data) {
                if(data.status==200){
                    var data={
                        qr_disc:data.data.description,
                        qr_scenes:data.data.dictSceneValue,
                        qr_valid:data.data.expiryDay==0?"永久":"短期",
                        start:data.data.createTimeStr,
                        end:data.data.expiryTimeStr,
                        qr_type:data.data.matterFwType=="0"?"文字":data.data.matterFwType=="1"?"图片":data.data.matterFwType=="2"?"图文":"无",
                        qr_content:data.data.matterFwContent,
                        qr:serverip+"iqrcode/web/download?fileName="+data.data.filePath+data.data.fileName,
                        qr_file:data.data.fileName
                    };
                    ReactDOM.render(
                        React.createElement(QRDetail, {name: "二维码详情", data: {id:id,data:data}}),
                        $('#newQR')[0]
                    );
                    $('#newQR').css({width:'100%'}).animate({height:'100%'},10);
                }
            }.bind(this)
        });
    },
    lookData: function (e) {
        var id=e.target.name;
        ReactDOM.render(
            React.createElement(QRdata, {name: "数据表", data: {id:id}}),
            $('#newQR')[0]
        );
        $('#newQR').css({width:'100%'}).animate({height:'100%'},10);
    },
    render: function(){
        return (
            React.createElement("div", {className: "task QRcode"}, 
                React.createElement("div", {id: "newQR"}), 
                React.createElement("div", {className: "taskTitle"}, "二维码管理"), 
                React.createElement("div", {className: "formContainer"}, 
                    React.createElement("label", null, "二维码说明："), React.createElement("input", {className: "taskName_input", ref: "qrDescrip"}), 
                    React.createElement("label", null, "投放场景："), 
                    React.createElement("select", {className: "taskPlatform_select", ref: "scenes"}, 
                        React.createElement("option", {key: -1, value: "全部"}, "全部"), 
                        
                            this.state.dictSceneList.map(function (value, i) {
                                return (
                                    React.createElement("option", {key: i, value: value.id}, value.value)
                                )
                            })
                        
                    ), 
                    React.createElement("label", null, "有效期："), 
                    React.createElement("select", {className: "taskPlatform_select", ref: "validPeriod"}, 
                        React.createElement("option", {value: "全部"}, "全部"), 
                        React.createElement("option", {value: "0"}, "永久"), 
                        React.createElement("option", {value: "-1"}, "临时")
                    ), 
                    React.createElement("input", {className: "isIncloude", name: "isIncloude", type: "checkbox", defaultChecked: this.state.isIncloude, onChange: this.changeCheck}), React.createElement("span", null, "包含已删除二维码")
                ), 
                React.createElement("div", {className: "formContainer"}, 
                    React.createElement("label", null, "创建时间："), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "creatTime", ref: "creatTime", type: "text"}), 
                    React.createElement("label", null, "关注时间："), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "export_start", ref: "export_start", type: "text"}), 
                    React.createElement("label", {style: {marginRight: '15px'}}, "－"), React.createElement("input", {readOnly: "readOnly", className: "time_input", id: "export_end", ref: "export_end", type: "text"}), 
                    React.createElement("button", {className: "searchButton QRsearch", onClick: this.searchData, style: {marginLeft:'20px'}}, "查询"), 
                    React.createElement("button", {className: "addBtn", onClick: this.newQR}, "新增二维码")
                ), 
                React.createElement("div", {className: "formContainer"}, 
                    React.createElement("a", {className: "TDaCss", onClick: this.batchDown}, "批量下载"), 
                    React.createElement("a", {className: "TDaCss", onClick: this.batchDel}, "批量删除"), 
                    React.createElement("a", {className: "TDaCss", onClick: this.batchOut}, "批量导出")
                ), 
                React.createElement("div", {className: "tableContainer"}, 
                    React.createElement("table", {className: "qrTable"}, 
                        React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, React.createElement("input", {type: "checkbox", checked: this.state.allData, name: "allData", onChange: this.changeCheck})), 
                            React.createElement("th", null, "序号"), 
                            React.createElement("th", null, "二维码名字"), 
                            React.createElement("th", null, "投放场景"), 
                            React.createElement("th", null, "有效期"), 
                            React.createElement("th", null, "创建时间"), 
                            React.createElement("th", null, "创建人"), 
                            React.createElement("th", null, "操作")
                        )
                        ), 
                        React.createElement("tbody", null, 
                        
                            this.state.data.data.map(function (value, i) {
                                var delCSS="TDaCss delQR";
                                var editCSS="TDaCss";
                                if(value.isDel=="T"){
                                    delCSS="TDaCss isdel";
                                    editCSS="TDaCss isdel";
                                }
                                return(
                                    React.createElement("tr", {key: i}, 
                                        React.createElement("td", null, React.createElement("input", {type: "checkbox", checked: this.state.checkedData[value.id], value: value.id, name: "itemData", onChange: this.changeCheck})), 
                                        React.createElement("td", null, (this.state.data.currentPage-1)*10+i+1), 
                                        React.createElement("td", null, value.description), 
                                        React.createElement("td", null, value.dictSceneValue), 
                                        React.createElement("td", null, value.expiryDay==0?"永久":value.expiryDay), 
                                        React.createElement("td", null, value.createTimeStr), 
                                        React.createElement("td", null, value.createUser), 
                                        React.createElement("td", null, 
                                            React.createElement("a", {className: "TDaCss", name: value.id, onClick: this.lookDetail}, "查看"), 
                                            React.createElement("a", {className: "TDaCss", name: value.id, onClick: this.lookData}, "数据"), 
                                            React.createElement("a", {className: editCSS, name: value.id, onClick: this.newQR}, "修改"), 
                                            React.createElement("a", {className: "TDaCss downFile", name: value.id, href: serverip+"iqrcode/web/download?fileName="+value.filePath+value.fileName, download: value.description+"_"+value.dictSceneValue+".jpg"}, "下载"), 
                                            React.createElement("a", {className: delCSS, name: value.id, onClick: this.delQR}, "删除")
                                        )
                                    )
                                );
                            }.bind(this))
                        
                        )
                    )
                ), 
                React.createElement(PageToggle, {totals: this.state.data.totals, currentPage: this.state.data.currentPage, callbackPage: this.selectPage})
            ));
    }
});


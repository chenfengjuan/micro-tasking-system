/**
 * Created by chenfengjuan on 17/3/28.
 */

var QRDetail=React.createClass({displayName: "QRDetail",
    backManage: function () {
        $('#newQR').animate({height:'0'},50,function () {
            $(this).css({width:'0'}).html("");
        });
    },
    getTimeCSS: function () {
        return this.props.data.data.qr_valid=="短期"?"rowTask":"rowTask none-box";
    },
    getTextCSS: function () {
        return this.props.data.data.qr_type=="文字"?"rowTask":"rowTask none-box";
    },
    getImgCSS: function () {
        return this.props.data.data.qr_type!="文字"?"rowTask":"rowTask none-box";
    },
    downQR: function (e) {

    },
    render: function(){
        var contentH=parseInt($(".QRcode").innerHeight())-51;
        return (
            React.createElement("div", {className: "taskDetailsTemplate QRDetail"}, 
                React.createElement("div", {className: "taskTitle"}, 
                    React.createElement("span", {className: "reviewT", onClick: this.backManage}, "二维码管理 > "), React.createElement("span", {className: "reviewT2"}, this.props.name), 
                    React.createElement("button", {className: "taskDetailsBack newButton", onClick: this.backManage}, "返回")), 
                React.createElement("div", {className: "taskDetailsContent", style: {height:contentH+'px'}}, 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("img", {className: "qrImg", src: this.props.data.data.qr})
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "文件名称:"), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.qr_file)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "二维码名字:"), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.qr_disc)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "投放场景:"), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.qr_scenes)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "有效期:"), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.qr_valid)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "创建时间:"), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.start)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "结束时间:"), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.end)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}, "推送素材:"), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.qr_type)
                    ), 
                    React.createElement("div", {className: "rowTask"}, 
                        React.createElement("span", {className: "titlesPosition titleQR"}), 
                        React.createElement("span", {className: "detailQR"}, this.props.data.data.qr_content)
                    ), 
                    /*<div className="rowTask">*/
                        /*<span className="titlesPosition titleQR">欢迎语:</span>*/
                        /*<span className="detailQR">{this.props.data.data.qr_welcome}</span>*/
                    /*</div>*/
                    React.createElement("div", {className: "rowTask", style: {marginLeft: '20px'}}, 
                        React.createElement("br", null), 
                        React.createElement("a", {href: this.props.data.data.qr, download: this.props.data.data.qr_disc+"_"+this.props.data.data.qr_disc+".jpg", className: "TDaCss downdetail"}, "下载")
                    )
                )
            ));
    }
});


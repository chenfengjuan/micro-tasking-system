/**
 * Created by jiayi.hu on 4/26/17.
 */
var CardManage = React.createClass({displayName: "CardManage",
    getInitialState: function () {
        return {
                totals: 1,
                currentPage: 1,
                taskBrandList:[],
                data: []
        }
    },
    requestTaskData: function (page) {
        console.log(page)
        var form=this.getFormData(page);
        console.log(form)
        $.ajax({
            url:serverip+"tereward/web/list",
            type:'get',
            data:form,
            success: function (res) {
                console.log(res)
                if(res.status==200){
                    var totals=res.data.total%res.data.pageSize==0?res.data.total/res.data.pageSize:parseInt(res.data.total/res.data.pageSize)+1;
                    console.log(totals)
                    this.setState({
                            totals:totals,
                            currentPage:res.data.pageNo,
                            data:res.data.rows
                        // allData:false
                    });
                }
            }.bind(this)
        });
    },
    componentDidMount: function () {
        console.log("run componentDidMount");
        $.ajax({
            url:serverip+"dict/web/getDictsByType?type=taskbrand",
            type:'get',
            success: function (res) {
                console.log(res);
                if(res.status==200){
                    this.setState({
                        taskBrandList: res.data
                    });
                }
            }.bind(this)
        });
        this.requestTaskData(1);
    },
    getFormData: function (args) {
        var formData = new Object();
        formData.elecName = ReactDOM.findDOMNode(this.refs.cardName).value;
        formData.taskName = ReactDOM.findDOMNode(this.refs.belongTask).value;
        formData.dictBrandId = ReactDOM.findDOMNode(this.refs.cardBrand).value;
        formData.pageNo = args != undefined ? parseInt(args) : 1;
        return formData;
    },
    newCard: function (e) {
        console.log("run newCard");
        console.log(this.state);
        if(!$(e.target).hasClass("isdel")) {
            var id=e.target.name==undefined?"":e.target.name;
            var name=id==""?"新增二维码":"修改二维码";
            ReactDOM.render(
                React.createElement(CardEdit, {name: name, data: {id:id,data:this.state.taskBrandList}, callback: this.requestTaskData}),
                $('#newCard')[0]
            );
            $('#newCard').css({width: '100%'}).animate({height: '100%'}, 10);
        }
    },
    lookState: function (e) {
        var id=e.target.name;
        $.ajax({
            url:serverip+'tereward/web/select/'+id,
            type:'get',
            success:function (res) {
               if(res.status==200){
                   console.log(res)
                   ReactDOM.render(
                       React.createElement(CardState, {name: "cardState", data: {data:res.data.description}}),
                       $('#cardModel')[0]
                   );
                   $('#cardModel').css({width:'100%'}).animate({height:'100%'},0);
               }
            }
        })
    },
    lookDetail: function (e,page) {
        var id=e.target.name;
        var pageNo = page != undefined ? page : 1;
        $.ajax({
            url:serverip+'tereward/web/selectElecRecord/'+pageNo+'/'+id,
            type:'get',
            success:function (res) {
                console.log(res)
               if(res.status==200){
                   ReactDOM.render(
                       React.createElement(CardDetail, {name: "cardDetail", data: {data:res.data,id:id}}),
                       $('#cardModel')[0]
                   );
                   $('#cardModel').css({width:'100%'}).animate({height:'100%'},0);
               }
            }
        })
    },
    dataDownload: function (e) {
        var id = e.target.name;
        window.location.href = serverip+'/elec/sysExport/exportElecRecord?idStr='+id;
    },
    selectPage: function (page) {
        this.requestTaskData(page);
    },
    searchPage: function () {
        this.requestTaskData(1);
    },
    render: function () {
        return (
            React.createElement("div", {className: "task cardManage"}, 
                React.createElement("div", {id: "newCard"}), 
                React.createElement("div", {id: "cardModel"}), 
                React.createElement("div", {className: "taskTitle"}, "卡券管理"), 
                React.createElement("div", {className: "formContainer"}, 
                    React.createElement("label", null, "卡券名称:  "), React.createElement("input", {className: "time_input", id: "", ref: "cardName", type: "text"}), 
                    React.createElement("label", null, "所属任务:  "), React.createElement("input", {className: "time_input", id: "", ref: "belongTask", type: "text"}), 
                    React.createElement("label", null, "卡券品牌:  "), 
                    /*<input className="time_input" id="" ref="cardBrand" type="text" />*/
                    React.createElement("select", {className: "taskPlatform_select selectAdd", readOnly: "readOnly", ref: "cardBrand"}, 
                        React.createElement("option", {key: -1, value: ""}, "全部"), 
                        
                            this.state.taskBrandList.map(function (value,i) {
                                return (
                                    React.createElement("option", {key: i, value: value.id}, value.value)
                                )
                            })
                        
                    ), 
                    React.createElement("button", {className: "searchButton", style: {marginLeft: '20px'}, onClick: this.searchPage}, "查询"), 
                    React.createElement("button", {className: "addBtn addCard", onClick: this.newCard}, "新增卡券")
                ), 
                /*<div className="formContainer">*/
                /*<a className="TDaCss" onClick={this.batchOut}>批量导出</a>*/
                /*</div>*/
                React.createElement("div", {className: "tableContainer"}, 
                    React.createElement("table", {className: "qrTable cardTable"}, 
                        React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", null, "编号"), 
                            React.createElement("th", null, "卡券名称"), 
                            React.createElement("th", null, "所属任务"), 
                            React.createElement("th", null, "卡券品牌"), 
                            React.createElement("th", null, "点击数量"), 
                            React.createElement("th", null, "点击人数"), 
                            React.createElement("th", null, "点击时间"), 
                            React.createElement("th", null, "操作")
                        )
                        ), 
                        React.createElement("tbody", null, 
                        
                            this.state.data.map(function (value, i) {
                                console.log(value)
                                var inputCss = "TDaCss";
                                if(value.taskName==null){
                                    inputCss = "TDaCssDisable";
                                }
                                return (
                                    React.createElement("tr", {key: i}, 
                                        React.createElement("td", null, (this.state.currentPage - 1) * 10 + i + 1), 
                                        React.createElement("td", null, value.elecName), 
                                        React.createElement("td", null, value.taskName), 
                                        React.createElement("td", null, value.dictBrandName), 
                                        React.createElement("td", null, value.clickTotal), 
                                        React.createElement("td", null, value.clickPersonTotal), 
                                        React.createElement("td", null, value.start2EndDateStr), 
                                        React.createElement("td", null, 
                                            React.createElement("input", {type: "button", className: "TDaCss", name: value.id, onClick: this.lookState, value: "说明"}), 
                                            React.createElement("input", {type: "button", disabled: value.taskName==null, className: inputCss, name: value.taskId, onClick: this.lookDetail, value: "详情"}), 
                                            React.createElement("input", {type: "button", className: "TDaCss", name: value.id, onClick: this.newCard, value: "修改"}), 
                                            React.createElement("input", {type: "button", disabled: value.taskName=null, className: inputCss, name: value.taskId, onClick: this.dataDownload, value: "数据导出"})
                                        )
                                    )
                                );
                            }.bind(this))
                        
                        )
                    )
                ), 
                React.createElement(PageToggle, {totals: this.state.totals, currentPage: this.state.currentPage, callbackPage: this.selectPage})
            )
        )
    }
})
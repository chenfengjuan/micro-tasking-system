/**
 * Created by chenfengjuan on 17/2/21.
 */
var Confirm=React.createClass({displayName: "Confirm",
    getInitialState: function () {
        return {
            firmW:194
        }
    },
    cancelConfirm: function (e) {
        $(e.target).remove();
    },
    stopPro: function (e) {
        e.stopPropagation();
    },
    cancelfun: function (e) {
        this.props.callback({e:this.props.data.event,msg:false});
    },
    okfun: function (e) {
        this.props.callback({e:this.props.data.event,msg:true});
    },
    getOkCss: function () {
        return this.props.data.css!=undefined?this.props.data.css:"confirmOk";
    },
    render: function () {
        var l=this.props.data.y-this.state.firmW;
        var t=this.props.data.x-27;
        return (
            React.createElement("div", {className: "maskDiv"}, 
                React.createElement("div", {className: "confirmDiv", ref: "firm", onClick: this.stopPro, style: {top:t+'px',left:l+'px'}}, 
                    React.createElement("div", {className: "containerConfirm"}, 
                        React.createElement("div", null, this.props.name), 
                        React.createElement("div", {className: "btnDiv"}, 
                            React.createElement("button", {className: "confirmCancel", onClick: this.cancelfun}, "取消"), 
                            React.createElement("button", {className: this.getOkCss(), onClick: this.okfun}, "确定")
                        )
                    ), 
                    React.createElement("div", {className: "confirmTriangle", style: {left:this.state.firmW-19+'px'}})
                )
            )
        );
    }
});
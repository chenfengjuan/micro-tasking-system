/**
 * Created by chenfengjuan on 17/1/18.
 */
$().ready(function () {
    var data=sessionStorage.getItem('name');
    if(data==200){
        new app();
    }else {
        window.location.href="index.html";
    }
    new app();
});
var serverip="https://mpmt.gemii.cc/mpmt/";
// var serverip="http://955d9892.ngrok.io/mpmt/";
// var serverip="http://test.gemii.cc/mpmt/";
// var serverip="http://192.168.0.57:8080/mpmt/";
var app=function () {
    this.initialize();
}
app.prototype={
    initialize: function () {
        ReactDOM.render(
            React.createElement(Content, null),
            $('.bodyContainer')[0]
        );
        this.setLayout();
        this.setEvents();
    },
    setLayout: function () {
        var bodyW=$(document.body).outerWidth();
        var bodyH=$(window).outerHeight();
        var minH=parseInt($('html').css('minHeight'));
        if(bodyH<=minH){
            bodyH=minH;
        }
        var headerH=$(".headerContainer").outerHeight();
        var sideW=$(".sideContainer").outerWidth();
        $(".bodyContainer").css({'height':bodyH-headerH+'px'});
        var padding=parseInt($(".contentContainer").css("padding"))*2;
        $(".contentContainer").css({'width':bodyW-sideW-padding+'px','height':bodyH-headerH-padding+'px'});
        $("#taskManagement").css({'maxHeight': bodyH-headerH-padding-202+'px'});
        $(".taskDetailsContent").css({height:bodyH-headerH-padding-51+'px'});
        $(".qrTable tbody,.qrDataTable tbody").css({'maxHeight': bodyH-headerH-padding-210+'px'});

    },
    setEvents: function () {
        var self=this;
        $(window).resize(this.setLayout);
        //配置时间控件
        $("#sTime,#eTime").jeDate({
            format:"YYYY-MM-DD hh:mm",
            zIndex:3000,
            ishmsVal:true,
            success:function(elem) {
                $("#jedatebox").hide();
                $(window).resize();
                var top=$(elem).offset().top+$(elem).outerHeight();
                var left=$(elem).offset().left;
                $("#jedatebox").css({top:top+'px',left:left+'px'});
                $("#jedatebox").show();
                $(".jedatehms").off("click");
                $(".jedatemm").off('click');
                $(".jedateyy").off('click');
                $(".jedatehms").find("li:nth-of-type(2)").find("input").val("00").attr("disabled","disabled");
                $(".jedatetodaymonth").remove();
                $(".jedateblue .jedatebot .jedatebtn span").css({width: '46%'});
                $(".pndrop").remove();
            }
        });
        $(".out").on("click",this.out);
    },
    out: function () {
        $.ajax({
            url: serverip+'sysuser/loginOut',
            type: 'get',
            success: function (data) {
                if(data.status==200){
                    window.location.href="login.html";
                }
            }
        });
    }
}